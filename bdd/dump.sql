-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: ghost
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesstokens`
--

DROP TABLE IF EXISTS `accesstokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesstokens` (
  `id` varchar(24) NOT NULL,
  `token` varchar(191) NOT NULL,
  `user_id` varchar(24) NOT NULL,
  `client_id` varchar(24) NOT NULL,
  `issued_by` varchar(24) DEFAULT NULL,
  `expires` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accesstokens_token_unique` (`token`),
  KEY `accesstokens_user_id_foreign` (`user_id`),
  KEY `accesstokens_client_id_foreign` (`client_id`),
  CONSTRAINT `accesstokens_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `accesstokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesstokens`
--

LOCK TABLES `accesstokens` WRITE;
/*!40000 ALTER TABLE `accesstokens` DISABLE KEYS */;
INSERT INTO `accesstokens` VALUES ('5e68ccd50b82c0000143c8b9','nvhKL3miMrgDbJa8e0uWUbMvIldevAlt0CstpOVruqwZRmiOD9Neb8rCzkIQFXqouB8z7zyboaeaothDeur1pzyIjPEmpZV8R67tSU4YeQNhgU8dbihjn41jbZDN8x9U37Oa9TWa2ideXNJHyNUw4EQ5P5meJNAAexB7p3nkLsF17zDYjCita5QsVavS525','1','5e68cb29b58cfd0017b57f7b','5e68ccd50b82c0000143c8b8',1586554485566),('5e68fc3cbd6094000111bc6e','6eqVaUXZmKDfrW9GfLW235wp62wlKCvt6A588kqloslrnGPBnW2EFAO2JfXtGxkBud0tf6qgBPwk1RR3YXTsbWdGjV1tr7yewhEBaGNIVWi99RItJYWF4ZnADPkg7fqaWYIlJ7BOuQ8jSyaWdnBMjtTDm0436qpRGfn9Dx3l7aYMGaaSBKGkioFrlMccYXQ','1','5e68cb29b58cfd0017b57f7b','5e68fc3cbd6094000111bc6d',1586566620685),('5e6905c928571c00011d77b4','2q5VanPEbNxY9vOCtQwtcLs13iRhdsQuIcDHAqacxKHhbFoKWqNnXOWcQrO8KV9KA9NRCK1lXLSbt3SMAxgGUw01UPAwbrWTfXMT6K5jbv6tZRalTY1h9kgSavthChvdSrpsQjJhrrRIT5LwETOfuQVyREAqHoA0zer5eaYZ1hFg1QljXg1e7yl9Ox9FmRp','1','5e68cb29b58cfd0017b57f7b','5e6905c928571c00011d77b3',1583943128271),('5e690cac28571c00011d77b5','D5n3csYw7szGGfRZc6Yg8QgjpyCpf0rCssa0C6bwddbuw2C1f83bloRABUOTsUyEk3HigtoapjmUOkXxc15v33dWIPHDWQJkKaJcJ0xqSLkQTCbsht4KKdoILUohWJcWXP2WoQ9losu21hPzZC4qYQMgPjAgWck5qx6PAKctWgLsjRlUXYIbdAHTqhnXLpj','1','5e68cb29b58cfd0017b57f7b','5e6905c928571c00011d77b3',1586570828269),('5e6913b728571c00011d77b7','U6Sdkw1XRYF2gc4mZ3XArcwavMiHb7SNBajfH2ECRRR9LNIRgLHgD3PlQNVzD92WnfzBM766Jis5Xe4C2j3kfmEIYoXUER1MMLNnxhRxfzdKlSRRcVGTqyD60BM88gzWu380JxKcUGh2lVTyhTDrW2swWtSyt4cuX0nplMyKaTyhrkrb6fUrIgt5lsu9RXj','1','5e68cb29b58cfd0017b57f7b','5e6913b728571c00011d77b6',1586572631378),('5e691e985816ef0001601d07','brlbOYSbeAl2vbX1ErUhHQIS0Cfnz4Vy2wB8ePBEz0OFAPwnLClHnGzMiSMmET7kIsGvkHP2FWnMmTyJwreVKkDAL93bWZK0FrKFBAmMvtbTNnm6a2tO1DiMufCoEoG6LYADuuqG3r7NCH8Ub8fCiw44wRgkNOc5vvgnfdffBlCv0e04PzDdOxSxmk7GwHY','1','5e68cb29b58cfd0017b57f7b','5e691e985816ef0001601d06',1583953106359),('5e6933a65816ef0001601d12','tUtvl1QtdtyCIIjMALMbdf6JSxhYKoP1e076yGjK6NHEzhXMo26Cog5S0S5bRoL2jvHJCuQ2cI5xuGyb4SCUfsXCVdhLX1ZLHrgFjIS1X9oweS4yb2ch3ltFroO4TwbuwYiSlMxNjDHr276SdEScUoGHZYgvEN2hpxESuffLA6sYblAsFPF5oGtsNnSfYgp','1','5e68cb29b58cfd0017b57f7b','5e691e985816ef0001601d06',1586580806357);
/*!40000 ALTER TABLE `accesstokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_fields`
--

DROP TABLE IF EXISTS `app_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_fields` (
  `id` varchar(24) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text,
  `type` varchar(50) NOT NULL DEFAULT 'html',
  `app_id` varchar(24) NOT NULL,
  `relatable_id` varchar(24) NOT NULL,
  `relatable_type` varchar(50) NOT NULL DEFAULT 'posts',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_fields_app_id_foreign` (`app_id`),
  CONSTRAINT `app_fields_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_fields`
--

LOCK TABLES `app_fields` WRITE;
/*!40000 ALTER TABLE `app_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_settings`
--

DROP TABLE IF EXISTS `app_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_settings` (
  `id` varchar(24) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text,
  `app_id` varchar(24) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_settings_key_unique` (`key`),
  KEY `app_settings_app_id_foreign` (`app_id`),
  CONSTRAINT `app_settings_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_settings`
--

LOCK TABLES `app_settings` WRITE;
/*!40000 ALTER TABLE `app_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps`
--

DROP TABLE IF EXISTS `apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps` (
  `id` varchar(24) NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `version` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'inactive',
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `apps_name_unique` (`name`),
  UNIQUE KEY `apps_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps`
--

LOCK TABLES `apps` WRITE;
/*!40000 ALTER TABLE `apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brute`
--

DROP TABLE IF EXISTS `brute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brute` (
  `key` varchar(191) NOT NULL,
  `firstRequest` bigint(20) NOT NULL,
  `lastRequest` bigint(20) NOT NULL,
  `lifetime` bigint(20) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brute`
--

LOCK TABLES `brute` WRITE;
/*!40000 ALTER TABLE `brute` DISABLE KEYS */;
INSERT INTO `brute` VALUES ('njTc7rIkevF18xwXWBOH97FaXr57v0pZRnWC+5/hayk=',1583952806338,1583952806338,1583956406341,1);
/*!40000 ALTER TABLE `brute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_trusted_domains`
--

DROP TABLE IF EXISTS `client_trusted_domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_trusted_domains` (
  `id` varchar(24) NOT NULL,
  `client_id` varchar(24) NOT NULL,
  `trusted_domain` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_trusted_domains_client_id_foreign` (`client_id`),
  CONSTRAINT `client_trusted_domains_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_trusted_domains`
--

LOCK TABLES `client_trusted_domains` WRITE;
/*!40000 ALTER TABLE `client_trusted_domains` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_trusted_domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` varchar(24) NOT NULL,
  `uuid` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `secret` varchar(191) NOT NULL,
  `redirection_uri` varchar(2000) DEFAULT NULL,
  `client_uri` varchar(2000) DEFAULT NULL,
  `auth_uri` varchar(2000) DEFAULT NULL,
  `logo` varchar(2000) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'development',
  `type` varchar(50) NOT NULL DEFAULT 'ua',
  `description` varchar(2000) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clients_name_unique` (`name`),
  UNIQUE KEY `clients_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES ('5e68cb29b58cfd0017b57f7b','a8e9e123-e385-49e6-945b-c76793bd6d22','Ghost Admin','ghost-admin','3c923639a92d',NULL,NULL,NULL,NULL,'enabled','ua',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f7c','8763cd47-da3d-4903-94e4-f32e3811528c','Ghost Frontend','ghost-frontend','2acc5796df2e',NULL,NULL,NULL,NULL,'enabled','ua',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f7d','a0542aff-a6b7-438f-b6b1-b35288151c4c','Ghost Scheduler','ghost-scheduler','56bbb034e251',NULL,NULL,NULL,NULL,'enabled','web',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f7e','f1b361c9-7c08-416d-9853-7d1a9cd94ac8','Ghost Backup','ghost-backup','df91194efb70',NULL,NULL,NULL,NULL,'enabled','web',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invites`
--

DROP TABLE IF EXISTS `invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invites` (
  `id` varchar(24) NOT NULL,
  `role_id` varchar(24) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `token` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `expires` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invites_token_unique` (`token`),
  UNIQUE KEY `invites_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invites`
--

LOCK TABLES `invites` WRITE;
/*!40000 ALTER TABLE `invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `version` varchar(70) NOT NULL,
  `currentVersion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `migrations_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'1-create-tables.js','init','1.26'),(2,'2-create-fixtures.js','init','1.26'),(3,'1-post-excerpt.js','1.3','1.26'),(4,'1-codeinjection-post.js','1.4','1.26'),(5,'1-og-twitter-post.js','1.5','1.26'),(6,'1-add-backup-client.js','1.7','1.26'),(7,'1-add-permissions-redirect.js','1.9','1.26'),(8,'1-custom-template-post.js','1.13','1.26'),(9,'2-theme-permissions.js','1.13','1.26'),(10,'1-add-webhooks-table.js','1.18','1.26'),(11,'1-webhook-permissions.js','1.19','1.26'),(12,'1-remove-settings-keys.js','1.20','1.26'),(13,'1-add-contributor-role.js','1.21','1.26'),(14,'1-multiple-authors-DDL.js','1.22','1.26'),(15,'1-multiple-authors-DML.js','1.22','1.26'),(16,'1-update-koenig-beta-html.js','1.25','1.26'),(17,'2-demo-post.js','1.25','1.26');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations_lock`
--

DROP TABLE IF EXISTS `migrations_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations_lock` (
  `lock_key` varchar(191) NOT NULL,
  `locked` tinyint(1) DEFAULT '0',
  `acquired_at` datetime DEFAULT NULL,
  `released_at` datetime DEFAULT NULL,
  UNIQUE KEY `migrations_lock_lock_key_unique` (`lock_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations_lock`
--

LOCK TABLES `migrations_lock` WRITE;
/*!40000 ALTER TABLE `migrations_lock` DISABLE KEYS */;
INSERT INTO `migrations_lock` VALUES ('km01',0,'2020-03-11 15:11:27','2020-03-11 15:11:27');
/*!40000 ALTER TABLE `migrations_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` varchar(24) NOT NULL,
  `name` varchar(50) NOT NULL,
  `object_type` varchar(50) NOT NULL,
  `action_type` varchar(50) NOT NULL,
  `object_id` varchar(24) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES ('5e68cb29b58cfd0017b57f84','Export database','db','exportContent',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f85','Import database','db','importContent',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f86','Delete all content','db','deleteAllContent',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f87','Send mail','mail','send',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f88','Browse notifications','notification','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f89','Add notifications','notification','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f8a','Delete notifications','notification','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f8b','Browse posts','post','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f8c','Read posts','post','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f8d','Edit posts','post','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f8e','Add posts','post','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f8f','Delete posts','post','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f90','Browse settings','setting','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f91','Read settings','setting','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f92','Edit settings','setting','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f93','Generate slugs','slug','generate',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f94','Browse tags','tag','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f95','Read tags','tag','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f96','Edit tags','tag','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f97','Add tags','tag','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f98','Delete tags','tag','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f99','Browse themes','theme','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f9a','Edit themes','theme','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f9b','Activate themes','theme','activate',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f9c','Upload themes','theme','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f9d','Download themes','theme','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f9e','Delete themes','theme','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f9f','Browse users','user','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa0','Read users','user','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa1','Edit users','user','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa2','Add users','user','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa3','Delete users','user','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa4','Assign a role','role','assign',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa5','Browse roles','role','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa6','Browse clients','client','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa7','Read clients','client','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa8','Edit clients','client','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fa9','Add clients','client','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57faa','Delete clients','client','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fab','Browse subscribers','subscriber','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fac','Read subscribers','subscriber','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fad','Edit subscribers','subscriber','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fae','Add subscribers','subscriber','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57faf','Delete subscribers','subscriber','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb0','Browse invites','invite','browse',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb1','Read invites','invite','read',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb2','Edit invites','invite','edit',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb3','Add invites','invite','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb4','Delete invites','invite','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb5','Download redirects','redirect','download',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb6','Upload redirects','redirect','upload',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb7','Add webhooks','webhook','add',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57fb8','Delete webhooks','webhook','destroy',NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_apps`
--

DROP TABLE IF EXISTS `permissions_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_apps` (
  `id` varchar(24) NOT NULL,
  `app_id` varchar(24) NOT NULL,
  `permission_id` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_apps`
--

LOCK TABLES `permissions_apps` WRITE;
/*!40000 ALTER TABLE `permissions_apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_roles`
--

DROP TABLE IF EXISTS `permissions_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_roles` (
  `id` varchar(24) NOT NULL,
  `role_id` varchar(24) NOT NULL,
  `permission_id` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_roles`
--

LOCK TABLES `permissions_roles` WRITE;
/*!40000 ALTER TABLE `permissions_roles` DISABLE KEYS */;
INSERT INTO `permissions_roles` VALUES ('5e68cb2ab58cfd0017b57fca','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f84'),('5e68cb2ab58cfd0017b57fcb','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f85'),('5e68cb2ab58cfd0017b57fcc','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f86'),('5e68cb2ab58cfd0017b57fcd','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f87'),('5e68cb2ab58cfd0017b57fce','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f88'),('5e68cb2ab58cfd0017b57fcf','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f89'),('5e68cb2ab58cfd0017b57fd0','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f8a'),('5e68cb2ab58cfd0017b57fd1','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f8b'),('5e68cb2ab58cfd0017b57fd2','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f8c'),('5e68cb2ab58cfd0017b57fd3','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f8d'),('5e68cb2ab58cfd0017b57fd4','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f8e'),('5e68cb2ab58cfd0017b57fd5','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f8f'),('5e68cb2ab58cfd0017b57fd6','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f90'),('5e68cb2ab58cfd0017b57fd7','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f91'),('5e68cb2ab58cfd0017b57fd8','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f92'),('5e68cb2ab58cfd0017b57fd9','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f93'),('5e68cb2ab58cfd0017b57fda','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f94'),('5e68cb2ab58cfd0017b57fdb','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f95'),('5e68cb2ab58cfd0017b57fdc','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f96'),('5e68cb2ab58cfd0017b57fdd','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f97'),('5e68cb2ab58cfd0017b57fde','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f98'),('5e68cb2ab58cfd0017b57fdf','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f99'),('5e68cb2ab58cfd0017b57fe0','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f9a'),('5e68cb2ab58cfd0017b57fe1','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f9b'),('5e68cb2ab58cfd0017b57fe2','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f9c'),('5e68cb2ab58cfd0017b57fe3','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f9d'),('5e68cb2ab58cfd0017b57fe4','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f9e'),('5e68cb2ab58cfd0017b57fe5','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57f9f'),('5e68cb2ab58cfd0017b57fe6','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa0'),('5e68cb2ab58cfd0017b57fe7','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa1'),('5e68cb2ab58cfd0017b57fe8','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa2'),('5e68cb2ab58cfd0017b57fe9','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa3'),('5e68cb2ab58cfd0017b57fea','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa4'),('5e68cb2ab58cfd0017b57feb','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa5'),('5e68cb2ab58cfd0017b57fec','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa6'),('5e68cb2ab58cfd0017b57fed','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa7'),('5e68cb2ab58cfd0017b57fee','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa8'),('5e68cb2ab58cfd0017b57fef','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fa9'),('5e68cb2ab58cfd0017b57ff0','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57faa'),('5e68cb2ab58cfd0017b57ff1','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fab'),('5e68cb2ab58cfd0017b57ff2','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fac'),('5e68cb2ab58cfd0017b57ff3','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fad'),('5e68cb2ab58cfd0017b57ff4','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fae'),('5e68cb2ab58cfd0017b57ff5','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57faf'),('5e68cb2ab58cfd0017b57ff6','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb0'),('5e68cb2ab58cfd0017b57ff7','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb1'),('5e68cb2ab58cfd0017b57ff8','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb2'),('5e68cb2ab58cfd0017b57ff9','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb3'),('5e68cb2ab58cfd0017b57ffa','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb4'),('5e68cb2ab58cfd0017b57ffb','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb5'),('5e68cb2ab58cfd0017b57ffc','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb6'),('5e68cb2ab58cfd0017b57ffd','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb7'),('5e68cb2ab58cfd0017b57ffe','5e68cb29b58cfd0017b57f7f','5e68cb29b58cfd0017b57fb8'),('5e68cb2ab58cfd0017b57fff','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f8b'),('5e68cb2ab58cfd0017b58000','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f8c'),('5e68cb2ab58cfd0017b58001','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f8d'),('5e68cb2ab58cfd0017b58002','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f8e'),('5e68cb2ab58cfd0017b58003','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f8f'),('5e68cb2ab58cfd0017b58004','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f90'),('5e68cb2ab58cfd0017b58005','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f91'),('5e68cb2ab58cfd0017b58006','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f93'),('5e68cb2ab58cfd0017b58007','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f94'),('5e68cb2ab58cfd0017b58008','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f95'),('5e68cb2ab58cfd0017b58009','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f96'),('5e68cb2ab58cfd0017b5800a','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f97'),('5e68cb2ab58cfd0017b5800b','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f98'),('5e68cb2ab58cfd0017b5800c','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f9f'),('5e68cb2ab58cfd0017b5800d','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa0'),('5e68cb2ab58cfd0017b5800e','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa1'),('5e68cb2ab58cfd0017b5800f','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa2'),('5e68cb2ab58cfd0017b58010','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa3'),('5e68cb2ab58cfd0017b58011','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa4'),('5e68cb2ab58cfd0017b58012','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa5'),('5e68cb2ab58cfd0017b58013','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa6'),('5e68cb2ab58cfd0017b58014','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa7'),('5e68cb2ab58cfd0017b58015','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa8'),('5e68cb2ab58cfd0017b58016','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fa9'),('5e68cb2ab58cfd0017b58017','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57faa'),('5e68cb2ab58cfd0017b58018','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fae'),('5e68cb2ab58cfd0017b58019','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fb0'),('5e68cb2ab58cfd0017b5801a','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fb1'),('5e68cb2ab58cfd0017b5801b','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fb2'),('5e68cb2ab58cfd0017b5801c','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fb3'),('5e68cb2ab58cfd0017b5801d','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57fb4'),('5e68cb2ab58cfd0017b5801e','5e68cb29b58cfd0017b57f80','5e68cb29b58cfd0017b57f99'),('5e68cb2ab58cfd0017b5801f','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f8b'),('5e68cb2ab58cfd0017b58020','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f8c'),('5e68cb2ab58cfd0017b58021','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f8e'),('5e68cb2ab58cfd0017b58022','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f90'),('5e68cb2ab58cfd0017b58023','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f91'),('5e68cb2ab58cfd0017b58024','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f93'),('5e68cb2ab58cfd0017b58025','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f94'),('5e68cb2ab58cfd0017b58026','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f95'),('5e68cb2ab58cfd0017b58027','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f97'),('5e68cb2ab58cfd0017b58028','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f9f'),('5e68cb2ab58cfd0017b58029','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fa0'),('5e68cb2ab58cfd0017b5802a','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fa5'),('5e68cb2ab58cfd0017b5802b','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fa6'),('5e68cb2ab58cfd0017b5802c','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fa7'),('5e68cb2ab58cfd0017b5802d','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fa8'),('5e68cb2ab58cfd0017b5802e','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fa9'),('5e68cb2ab58cfd0017b5802f','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57faa'),('5e68cb2ab58cfd0017b58030','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57fae'),('5e68cb2ab58cfd0017b58031','5e68cb29b58cfd0017b57f81','5e68cb29b58cfd0017b57f99'),('5e68cb2ab58cfd0017b58032','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f8b'),('5e68cb2ab58cfd0017b58033','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f8c'),('5e68cb2ab58cfd0017b58034','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f8e'),('5e68cb2ab58cfd0017b58035','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f90'),('5e68cb2ab58cfd0017b58036','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f91'),('5e68cb2ab58cfd0017b58037','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f93'),('5e68cb2ab58cfd0017b58038','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f94'),('5e68cb2ab58cfd0017b58039','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f95'),('5e68cb2ab58cfd0017b5803a','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f9f'),('5e68cb2ab58cfd0017b5803b','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fa0'),('5e68cb2ab58cfd0017b5803c','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fa5'),('5e68cb2ab58cfd0017b5803d','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fa6'),('5e68cb2ab58cfd0017b5803e','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fa7'),('5e68cb2ab58cfd0017b5803f','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fa8'),('5e68cb2ab58cfd0017b58040','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fa9'),('5e68cb2ab58cfd0017b58041','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57faa'),('5e68cb2ab58cfd0017b58042','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57fae'),('5e68cb2ab58cfd0017b58043','5e68cb29b58cfd0017b57f82','5e68cb29b58cfd0017b57f99');
/*!40000 ALTER TABLE `permissions_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_users`
--

DROP TABLE IF EXISTS `permissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_users` (
  `id` varchar(24) NOT NULL,
  `user_id` varchar(24) NOT NULL,
  `permission_id` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_users`
--

LOCK TABLES `permissions_users` WRITE;
/*!40000 ALTER TABLE `permissions_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` varchar(24) NOT NULL,
  `uuid` varchar(36) NOT NULL,
  `title` varchar(2000) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `mobiledoc` longtext,
  `html` longtext,
  `amp` longtext,
  `plaintext` longtext,
  `feature_image` varchar(2000) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `page` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT 'draft',
  `locale` varchar(6) DEFAULT NULL,
  `visibility` varchar(50) NOT NULL DEFAULT 'public',
  `meta_title` varchar(2000) DEFAULT NULL,
  `meta_description` varchar(2000) DEFAULT NULL,
  `author_id` varchar(24) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `published_by` varchar(24) DEFAULT NULL,
  `custom_excerpt` varchar(2000) DEFAULT NULL,
  `codeinjection_head` text,
  `codeinjection_foot` text,
  `og_image` varchar(2000) DEFAULT NULL,
  `og_title` varchar(300) DEFAULT NULL,
  `og_description` varchar(500) DEFAULT NULL,
  `twitter_image` varchar(2000) DEFAULT NULL,
  `twitter_title` varchar(300) DEFAULT NULL,
  `twitter_description` varchar(500) DEFAULT NULL,
  `custom_template` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES ('5e69152028571c00011d77b8','2fabed7c-5ec5-4df2-969d-2f3248e04266','[FRANDROID] SHADOW VS GEFORCE NOW','shadow-vs-geforce-now','{\"version\":\"0.3.1\",\"markups\":[],\"atoms\":[],\"cards\":[[\"card-markdown\",{\"cardName\":\"card-markdown\",\"markdown\":\"[Arctile de FRANDROID](https://www.frandroid.com/versus/678785_shadow-vs-geforce-now), comparant Shadow et la solution de Nvidia sur le marché du cloud gaming.\\n\\nLe cloud gaming prend petit à petit son envol et les offres se multiplient, envoyant de plus en plus nos jeux dans les nuages. Les avantages sont nombreux, permettant notamment aux propriétaires des plus petites configurations de profiter des meilleures performances sans avoir à changer d’appareil.\\n\\nParmi les offres, on peut citer Google Stadia, Microsoft xCloud, mais aussi Nvidia GeForce Now et Shadow*, de la start-up française Blade. Comparons ces deux offres point par point afin de déterminer laquelle est la meilleure pour répondre à vos besoins.\\n\\nPour aller plus loin\\nGoogle Stadia, GeForce Now, Shadow, xCloud, PS Now : quelle est la meilleure offre de cloud gaming ?\\n\\nD’un côté, GeForce Now (anciennement Grid) est l’un des plus anciens services de cloud gaming disponibles sur le marché, dont les offres commerciales ont été lancées en début d’année 2020. De l’autre côté, Shadow est un service de cloud computing français lancé en 2017, mais axe la plupart de sa communication autour du jeu.\\nDes promesses différentes\\n\\nLes deux services promettent donc des solutions différentes. D’un côté on a une expérience exclusivement gaming, tandis que de l’autre on a un PC complet dans les nuages, avec ce que cela apporte d’avantages et d’inconvénients. Les deux promesses se rejoignent néanmoins sur un point : les jeux compatibles sont issus de bibliothèques déjà existantes (Steam, Epic Store, etc.) et n’ont donc pas besoin d’être rachetés.\\n\\nSur ce point, Shadow prend donc de l’avance, puisqu’il est possible d’en faire plus, y compris de remplacer votre ordinateur quotidien sur d’autres tâches qui peuvent se montrer gourmandes, comme de la retouche photo par exemple. Mais cela a aussi un contrecoup : la gestion de l’espace de stockage. Selon l’offre, vous disposez de 256 Go à 1 To de stockage, ce qui peut rapidement se montrer limité lorsqu’il s’agit d’installer des jeux récents qui se comptent en centaines de Go. De son côté, GeForce Now est plus simple. Choisissez votre jeu, cliquez, jouez.\\n\\nGeForce Now montre également une autre contrainte : le temps de jeu continu. Avec l’offre gratuite, vous ne dépasserez pas l’heure de jeu, tandis que l’offre payante poussera à 6 heures en continu. Cette limite ne devrait certainement pas être trop limitante, mais elle pourrait poser problème à plus gros joueurs qui enchaînent les parties en ligne.\\nUn accès à votre ludothèque… ou presque\\n\\nSur le papier, les deux services proposent d’accéder à vos bibliothèques de jeux déjà existantes afin de ne pas avoir à acheter de nouveau des jeux que vous auriez déjà. C’est totalement le cas sur Shadow qui donne accès à un ordinateur complet. Vous retrouverez donc la totalité de votre ludothèque virtuelle, qu’elle soit sur Steam, Epic, GoG, ou téléchargée sur un site obscur de jeux indés slovaques.\\n\\nSur GeForce Now, c’est un peu plus compliqué. Les jeux doivent provenir de certaines sources seulement et les jeux doivent être optimisés pour. Certains éditeurs peuvent donc décider de retirer leurs jeux de la plateforme, quand ils ne sont pas tout simplement incompatibles. Par ailleurs, un jeu optimisé pour GeForce Now sur une plateforme ne l’est pas forcément sur une autre. Votre jeu préféré peut donc apparaître dans la liste de GeForce Now, mais uniquement dans sa version Steam alors que vous l’avez dans votre bibliothèque Epic, ou vice-versa.\\n\\nNiveau ludothèque, Shadow est donc largement devant.\\nPerformances graphiques et réseau\\n\\nLes performances d’un service de cloud gaming ne sont pas évidentes à mesurer ou à mettre en valeur tant elles dépendent de différents facteurs pouvant varier suivant les situations. Entre la stabilité de votre connexion, les interconnexions entre les différents nœuds parcourus par l’information, ou encore la distance avec le serveur sur lequel vous êtes connectés, l’expérience peut changer du tout au tout.\\n\\nMalgré cela, j’ai eu l’occasion de mettre à mal les deux services en les essayant dans des conditions très variées, et parfois très défavorables, permettant de noter les différences de gestion de la connexion sur GeForce Now et Shadow lorsque la connexion est loin d’être parfaite.\\n\\nTout d’abord, notons que dans d’excellentes conditions, avec un réseau fibre stable connecté en Ethernet, les deux services proposent une expérience digne de ce que l’on pourrait obtenir en local. Les plus pointilleux mesureront la latence à la demi frame près, mais il est clair que même les joueurs professionnels exigeants pourront s’entraîner dans ces conditions.\\n\\nC’est bien dans les conditions plus difficiles, en 4G ou en Wifi, qu’on voit les différences entre Shadow et GeForce Now. D’un côté Shadow va chercher à améliorer l’expérience vidéo pour rendre le flux le plus fluide possible, de l’autre GeForce Now privilégie les inputs, permettant d’avoir une meilleure réactivité des contrôles, parfois au détriment du nombre d’images par seconde.\\n\\nCe choix n’est pas anodin, car l’expérience va être impactée lorsque les conditions ne sont pas excellentes. Ainsi, en Wifi 5 GHz sur ma connexion fibre, un même jeu testé sur un Pixel 4 XL a montré un framerate bien plus élevé sur Shadow que sur GeForce Now. En 4G, les sensations étaient plus dégradées dans les deux cas, mais GeForce Now offre plus de réactivité avec des inputs qui sont pris en compte à tout moment.\\n\\nSur ce point, il est donc difficile de départager les deux services, d’autant que les infrastructures vont certainement évoluer au fil du temps (elles l’ont déjà fait dans les deux cas depuis leurs balbutiements).\\n\\nPour la partie graphique, GeForce Now est limité à du 1080p à 60 fps, avec ajout du ray-tracing sur l’offre payante. De son côté, Shadow propose trois offres, l’une en Full HD à 60 fps et les autres pouvant monter jusqu’en 4K.\\nAppareils compatibles et interface\\n\\nShadow comme Geforce Now peuvent tourner sur de nombreuses plateformes, depuis des ordinateurs (Windows ou Mac), smartphone et tablettes (Android, les versions iOS ayant été retirées de l’App Store dans les deux cas par Apple) ou TV connecté. Des interfaces spéciales ont été développées pour les smartphones et Android TV pour faciliter l’accès aux jeux. GeForce Now ajoute en plus de cela un layout de touches tactiles pour simuler une manette au besoin.\\n\\nGeForce Now propose une intégration particulièrement poussée sur la Shield TV, ce qui est très agréable à l’usage. Dans un cas comme dans l’autre, on est néanmoins souvent confrontés à des problèmes liés à la jeunesse du service. Se connecter à tous ses services sur GeForce Now lors du premier usage sur smartphone est un calvaire, Shadow sur TV ou smartphone ne donne pas de message d’erreur clair lorsqu’un jeu ne se lance pas à cause d’un problème de connexion à Windows (la fameuse mise à jour en arrière-plan qui bloque tout) et les deux peinent à prendre en compte la disposition du clavier de macOS, empêchant de rentrer des « @ ».\\n\\n\\nShadow offre trois offres allant de 15 à 50 euros en fonction des besoins. GeForce Now de son côté propose une offre gratuite (limitée à une heure de jeu en continu) et une offre avec du ray tracing et une limitation repoussée à 6h en continu.\\n\\n\\nSur ce point, GeForce Now est donc clairement le meilleur rapport qualité/prix puisque vous avez accès à un grand nombre de jeux en cloud gaming pour le tiers, voire le sixième du prix d’un abonnement Shadow.\\n\\nShadow est néanmoins beaucoup plus permissif et permet de faire beaucoup plus de choses. Si le prix n’est pas un frein pour vous, nous vous conseillons donc plutôt d’opter pour une offre de la start-up française.\\n\\n *Ulrich Rozier, cofondateur d’Humanoid, la société éditrice de Frandroid, est investisseur minoritaire à titre personnel de Shadow. L’avis de la rédaction n’est pas influencé.\\n\\n\\n\"}]],\"sections\":[[10,0]]}','<div class=\"kg-card-markdown\"><p><a href=\"https://www.frandroid.com/versus/678785_shadow-vs-geforce-now\">Arctile de FRANDROID</a>, comparant Shadow et la solution de Nvidia sur le marché du cloud gaming.</p>\n<p>Le cloud gaming prend petit à petit son envol et les offres se multiplient, envoyant de plus en plus nos jeux dans les nuages. Les avantages sont nombreux, permettant notamment aux propriétaires des plus petites configurations de profiter des meilleures performances sans avoir à changer d’appareil.</p>\n<p>Parmi les offres, on peut citer Google Stadia, Microsoft xCloud, mais aussi Nvidia GeForce Now et Shadow*, de la start-up française Blade. Comparons ces deux offres point par point afin de déterminer laquelle est la meilleure pour répondre à vos besoins.</p>\n<p>Pour aller plus loin<br>\nGoogle Stadia, GeForce Now, Shadow, xCloud, PS Now : quelle est la meilleure offre de cloud gaming ?</p>\n<p>D’un côté, GeForce Now (anciennement Grid) est l’un des plus anciens services de cloud gaming disponibles sur le marché, dont les offres commerciales ont été lancées en début d’année 2020. De l’autre côté, Shadow est un service de cloud computing français lancé en 2017, mais axe la plupart de sa communication autour du jeu.<br>\nDes promesses différentes</p>\n<p>Les deux services promettent donc des solutions différentes. D’un côté on a une expérience exclusivement gaming, tandis que de l’autre on a un PC complet dans les nuages, avec ce que cela apporte d’avantages et d’inconvénients. Les deux promesses se rejoignent néanmoins sur un point : les jeux compatibles sont issus de bibliothèques déjà existantes (Steam, Epic Store, etc.) et n’ont donc pas besoin d’être rachetés.</p>\n<p>Sur ce point, Shadow prend donc de l’avance, puisqu’il est possible d’en faire plus, y compris de remplacer votre ordinateur quotidien sur d’autres tâches qui peuvent se montrer gourmandes, comme de la retouche photo par exemple. Mais cela a aussi un contrecoup : la gestion de l’espace de stockage. Selon l’offre, vous disposez de 256 Go à 1 To de stockage, ce qui peut rapidement se montrer limité lorsqu’il s’agit d’installer des jeux récents qui se comptent en centaines de Go. De son côté, GeForce Now est plus simple. Choisissez votre jeu, cliquez, jouez.</p>\n<p>GeForce Now montre également une autre contrainte : le temps de jeu continu. Avec l’offre gratuite, vous ne dépasserez pas l’heure de jeu, tandis que l’offre payante poussera à 6 heures en continu. Cette limite ne devrait certainement pas être trop limitante, mais elle pourrait poser problème à plus gros joueurs qui enchaînent les parties en ligne.<br>\nUn accès à votre ludothèque… ou presque</p>\n<p>Sur le papier, les deux services proposent d’accéder à vos bibliothèques de jeux déjà existantes afin de ne pas avoir à acheter de nouveau des jeux que vous auriez déjà. C’est totalement le cas sur Shadow qui donne accès à un ordinateur complet. Vous retrouverez donc la totalité de votre ludothèque virtuelle, qu’elle soit sur Steam, Epic, GoG, ou téléchargée sur un site obscur de jeux indés slovaques.</p>\n<p>Sur GeForce Now, c’est un peu plus compliqué. Les jeux doivent provenir de certaines sources seulement et les jeux doivent être optimisés pour. Certains éditeurs peuvent donc décider de retirer leurs jeux de la plateforme, quand ils ne sont pas tout simplement incompatibles. Par ailleurs, un jeu optimisé pour GeForce Now sur une plateforme ne l’est pas forcément sur une autre. Votre jeu préféré peut donc apparaître dans la liste de GeForce Now, mais uniquement dans sa version Steam alors que vous l’avez dans votre bibliothèque Epic, ou vice-versa.</p>\n<p>Niveau ludothèque, Shadow est donc largement devant.<br>\nPerformances graphiques et réseau</p>\n<p>Les performances d’un service de cloud gaming ne sont pas évidentes à mesurer ou à mettre en valeur tant elles dépendent de différents facteurs pouvant varier suivant les situations. Entre la stabilité de votre connexion, les interconnexions entre les différents nœuds parcourus par l’information, ou encore la distance avec le serveur sur lequel vous êtes connectés, l’expérience peut changer du tout au tout.</p>\n<p>Malgré cela, j’ai eu l’occasion de mettre à mal les deux services en les essayant dans des conditions très variées, et parfois très défavorables, permettant de noter les différences de gestion de la connexion sur GeForce Now et Shadow lorsque la connexion est loin d’être parfaite.</p>\n<p>Tout d’abord, notons que dans d’excellentes conditions, avec un réseau fibre stable connecté en Ethernet, les deux services proposent une expérience digne de ce que l’on pourrait obtenir en local. Les plus pointilleux mesureront la latence à la demi frame près, mais il est clair que même les joueurs professionnels exigeants pourront s’entraîner dans ces conditions.</p>\n<p>C’est bien dans les conditions plus difficiles, en 4G ou en Wifi, qu’on voit les différences entre Shadow et GeForce Now. D’un côté Shadow va chercher à améliorer l’expérience vidéo pour rendre le flux le plus fluide possible, de l’autre GeForce Now privilégie les inputs, permettant d’avoir une meilleure réactivité des contrôles, parfois au détriment du nombre d’images par seconde.</p>\n<p>Ce choix n’est pas anodin, car l’expérience va être impactée lorsque les conditions ne sont pas excellentes. Ainsi, en Wifi 5 GHz sur ma connexion fibre, un même jeu testé sur un Pixel 4 XL a montré un framerate bien plus élevé sur Shadow que sur GeForce Now. En 4G, les sensations étaient plus dégradées dans les deux cas, mais GeForce Now offre plus de réactivité avec des inputs qui sont pris en compte à tout moment.</p>\n<p>Sur ce point, il est donc difficile de départager les deux services, d’autant que les infrastructures vont certainement évoluer au fil du temps (elles l’ont déjà fait dans les deux cas depuis leurs balbutiements).</p>\n<p>Pour la partie graphique, GeForce Now est limité à du 1080p à 60 fps, avec ajout du ray-tracing sur l’offre payante. De son côté, Shadow propose trois offres, l’une en Full HD à 60 fps et les autres pouvant monter jusqu’en 4K.<br>\nAppareils compatibles et interface</p>\n<p>Shadow comme Geforce Now peuvent tourner sur de nombreuses plateformes, depuis des ordinateurs (Windows ou Mac), smartphone et tablettes (Android, les versions iOS ayant été retirées de l’App Store dans les deux cas par Apple) ou TV connecté. Des interfaces spéciales ont été développées pour les smartphones et Android TV pour faciliter l’accès aux jeux. GeForce Now ajoute en plus de cela un layout de touches tactiles pour simuler une manette au besoin.</p>\n<p>GeForce Now propose une intégration particulièrement poussée sur la Shield TV, ce qui est très agréable à l’usage. Dans un cas comme dans l’autre, on est néanmoins souvent confrontés à des problèmes liés à la jeunesse du service. Se connecter à tous ses services sur GeForce Now lors du premier usage sur smartphone est un calvaire, Shadow sur TV ou smartphone ne donne pas de message d’erreur clair lorsqu’un jeu ne se lance pas à cause d’un problème de connexion à Windows (la fameuse mise à jour en arrière-plan qui bloque tout) et les deux peinent à prendre en compte la disposition du clavier de macOS, empêchant de rentrer des « @ ».</p>\n<p>Shadow offre trois offres allant de 15 à 50 euros en fonction des besoins. GeForce Now de son côté propose une offre gratuite (limitée à une heure de jeu en continu) et une offre avec du ray tracing et une limitation repoussée à 6h en continu.</p>\n<p>Sur ce point, GeForce Now est donc clairement le meilleur rapport qualité/prix puisque vous avez accès à un grand nombre de jeux en cloud gaming pour le tiers, voire le sixième du prix d’un abonnement Shadow.</p>\n<p>Shadow est néanmoins beaucoup plus permissif et permet de faire beaucoup plus de choses. Si le prix n’est pas un frein pour vous, nous vous conseillons donc plutôt d’opter pour une offre de la start-up française.</p>\n<p>*Ulrich Rozier, cofondateur d’Humanoid, la société éditrice de Frandroid, est investisseur minoritaire à titre personnel de Shadow. L’avis de la rédaction n’est pas influencé.</p>\n</div>',NULL,'Arctile de FRANDROID\n[https://www.frandroid.com/versus/678785_shadow-vs-geforce-now], comparant\nShadow et la solution de Nvidia sur le marché du cloud gaming.\n\nLe cloud gaming prend petit à petit son envol et les offres se multiplient,\nenvoyant de plus en plus nos jeux dans les nuages. Les avantages sont nombreux,\npermettant notamment aux propriétaires des plus petites configurations de\nprofiter des meilleures performances sans avoir à changer d’appareil.\n\nParmi les offres, on peut citer Google Stadia, Microsoft xCloud, mais aussi\nNvidia GeForce Now et Shadow*, de la start-up française Blade. Comparons ces\ndeux offres point par point afin de déterminer laquelle est la meilleure pour\nrépondre à vos besoins.\n\nPour aller plus loin\nGoogle Stadia, GeForce Now, Shadow, xCloud, PS Now : quelle est la meilleure\noffre de cloud gaming ?\n\nD’un côté, GeForce Now (anciennement Grid) est l’un des plus anciens services de\ncloud gaming disponibles sur le marché, dont les offres commerciales ont été\nlancées en début d’année 2020. De l’autre côté, Shadow est un service de cloud\ncomputing français lancé en 2017, mais axe la plupart de sa communication autour\ndu jeu.\nDes promesses différentes\n\nLes deux services promettent donc des solutions différentes. D’un côté on a une\nexpérience exclusivement gaming, tandis que de l’autre on a un PC complet dans\nles nuages, avec ce que cela apporte d’avantages et d’inconvénients. Les deux\npromesses se rejoignent néanmoins sur un point : les jeux compatibles sont issus\nde bibliothèques déjà existantes (Steam, Epic Store, etc.) et n’ont donc pas\nbesoin d’être rachetés.\n\nSur ce point, Shadow prend donc de l’avance, puisqu’il est possible d’en faire\nplus, y compris de remplacer votre ordinateur quotidien sur d’autres tâches qui\npeuvent se montrer gourmandes, comme de la retouche photo par exemple. Mais cela\na aussi un contrecoup : la gestion de l’espace de stockage. Selon l’offre, vous\ndisposez de 256 Go à 1 To de stockage, ce qui peut rapidement se montrer limité\nlorsqu’il s’agit d’installer des jeux récents qui se comptent en centaines de\nGo. De son côté, GeForce Now est plus simple. Choisissez votre jeu, cliquez,\njouez.\n\nGeForce Now montre également une autre contrainte : le temps de jeu continu.\nAvec l’offre gratuite, vous ne dépasserez pas l’heure de jeu, tandis que l’offre\npayante poussera à 6 heures en continu. Cette limite ne devrait certainement pas\nêtre trop limitante, mais elle pourrait poser problème à plus gros joueurs qui\nenchaînent les parties en ligne.\nUn accès à votre ludothèque… ou presque\n\nSur le papier, les deux services proposent d’accéder à vos bibliothèques de jeux\ndéjà existantes afin de ne pas avoir à acheter de nouveau des jeux que vous\nauriez déjà. C’est totalement le cas sur Shadow qui donne accès à un ordinateur\ncomplet. Vous retrouverez donc la totalité de votre ludothèque virtuelle,\nqu’elle soit sur Steam, Epic, GoG, ou téléchargée sur un site obscur de jeux\nindés slovaques.\n\nSur GeForce Now, c’est un peu plus compliqué. Les jeux doivent provenir de\ncertaines sources seulement et les jeux doivent être optimisés pour. Certains\néditeurs peuvent donc décider de retirer leurs jeux de la plateforme, quand ils\nne sont pas tout simplement incompatibles. Par ailleurs, un jeu optimisé pour\nGeForce Now sur une plateforme ne l’est pas forcément sur une autre. Votre jeu\npréféré peut donc apparaître dans la liste de GeForce Now, mais uniquement dans\nsa version Steam alors que vous l’avez dans votre bibliothèque Epic, ou\nvice-versa.\n\nNiveau ludothèque, Shadow est donc largement devant.\nPerformances graphiques et réseau\n\nLes performances d’un service de cloud gaming ne sont pas évidentes à mesurer ou\nà mettre en valeur tant elles dépendent de différents facteurs pouvant varier\nsuivant les situations. Entre la stabilité de votre connexion, les\ninterconnexions entre les différents nœuds parcourus par l’information, ou\nencore la distance avec le serveur sur lequel vous êtes connectés, l’expérience\npeut changer du tout au tout.\n\nMalgré cela, j’ai eu l’occasion de mettre à mal les deux services en les\nessayant dans des conditions très variées, et parfois très défavorables,\npermettant de noter les différences de gestion de la connexion sur GeForce Now\net Shadow lorsque la connexion est loin d’être parfaite.\n\nTout d’abord, notons que dans d’excellentes conditions, avec un réseau fibre\nstable connecté en Ethernet, les deux services proposent une expérience digne de\nce que l’on pourrait obtenir en local. Les plus pointilleux mesureront la\nlatence à la demi frame près, mais il est clair que même les joueurs\nprofessionnels exigeants pourront s’entraîner dans ces conditions.\n\nC’est bien dans les conditions plus difficiles, en 4G ou en Wifi, qu’on voit les\ndifférences entre Shadow et GeForce Now. D’un côté Shadow va chercher à\naméliorer l’expérience vidéo pour rendre le flux le plus fluide possible, de\nl’autre GeForce Now privilégie les inputs, permettant d’avoir une meilleure\nréactivité des contrôles, parfois au détriment du nombre d’images par seconde.\n\nCe choix n’est pas anodin, car l’expérience va être impactée lorsque les\nconditions ne sont pas excellentes. Ainsi, en Wifi 5 GHz sur ma connexion fibre,\nun même jeu testé sur un Pixel 4 XL a montré un framerate bien plus élevé sur\nShadow que sur GeForce Now. En 4G, les sensations étaient plus dégradées dans\nles deux cas, mais GeForce Now offre plus de réactivité avec des inputs qui sont\npris en compte à tout moment.\n\nSur ce point, il est donc difficile de départager les deux services, d’autant\nque les infrastructures vont certainement évoluer au fil du temps (elles l’ont\ndéjà fait dans les deux cas depuis leurs balbutiements).\n\nPour la partie graphique, GeForce Now est limité à du 1080p à 60 fps, avec ajout\ndu ray-tracing sur l’offre payante. De son côté, Shadow propose trois offres,\nl’une en Full HD à 60 fps et les autres pouvant monter jusqu’en 4K.\nAppareils compatibles et interface\n\nShadow comme Geforce Now peuvent tourner sur de nombreuses plateformes, depuis\ndes ordinateurs (Windows ou Mac), smartphone et tablettes (Android, les versions\niOS ayant été retirées de l’App Store dans les deux cas par Apple) ou TV\nconnecté. Des interfaces spéciales ont été développées pour les smartphones et\nAndroid TV pour faciliter l’accès aux jeux. GeForce Now ajoute en plus de cela\nun layout de touches tactiles pour simuler une manette au besoin.\n\nGeForce Now propose une intégration particulièrement poussée sur la Shield TV,\nce qui est très agréable à l’usage. Dans un cas comme dans l’autre, on est\nnéanmoins souvent confrontés à des problèmes liés à la jeunesse du service. Se\nconnecter à tous ses services sur GeForce Now lors du premier usage sur\nsmartphone est un calvaire, Shadow sur TV ou smartphone ne donne pas de message\nd’erreur clair lorsqu’un jeu ne se lance pas à cause d’un problème de connexion\nà Windows (la fameuse mise à jour en arrière-plan qui bloque tout) et les deux\npeinent à prendre en compte la disposition du clavier de macOS, empêchant de\nrentrer des « @ ».\n\nShadow offre trois offres allant de 15 à 50 euros en fonction des besoins.\nGeForce Now de son côté propose une offre gratuite (limitée à une heure de jeu\nen continu) et une offre avec du ray tracing et une limitation repoussée à 6h en\ncontinu.\n\nSur ce point, GeForce Now est donc clairement le meilleur rapport qualité/prix\npuisque vous avez accès à un grand nombre de jeux en cloud gaming pour le tiers,\nvoire le sixième du prix d’un abonnement Shadow.\n\nShadow est néanmoins beaucoup plus permissif et permet de faire beaucoup plus de\nchoses. Si le prix n’est pas un frein pour vous, nous vous conseillons donc\nplutôt d’opter pour une offre de la start-up française.\n\n*Ulrich Rozier, cofondateur d’Humanoid, la société éditrice de Frandroid, est\ninvestisseur minoritaire à titre personnel de Shadow. L’avis de la rédaction\nn’est pas influencé.','/content/images/2020/03/shadow-vs-gefoce-now-1200x674.jpg',0,0,'published',NULL,'public',NULL,NULL,'5951f5fca366002ebd5dbef7','2020-03-11 16:43:12','1','2020-03-11 17:50:27','1','2020-03-11 16:46:35','1',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e691f925816ef0001601d08','4231809c-705c-420b-a69a-3ba4f6ae8f03','[NUMERAMA] 17 points à savoir sur Shadow, Cloud Gaming français','tes','{\"version\":\"0.3.1\",\"markups\":[],\"atoms\":[],\"cards\":[[\"card-markdown\",{\"cardName\":\"card-markdown\",\"markdown\":\"Voici une présentation générale par Numerama du cloud computing français Shadow. [Lien de l\'article](https://www.numerama.com/tech/203045-shadow-tout-savoir-sur-le-pc-du-futur-en-19-questions.html).\\n\\nAlors que le cloud gaming commence à prendre plusieurs formes chez les géants de la tech, les Français de Shadow avancent. Fin 2019, l\'entreprise a bouleversé ses offres pour conquérir le grand public. Pour comprendre comment fonctionne ce PC dans le cloud étonnant, nous vous proposons une FAQ complète.\\n\\nDossier mis à jour le 19/04/2019.\\nShadow : Qu’est-ce que c’est ?\\n\\nSimple : un ordinateur sous Windows 10, à distance, dans le cloud, pensé pour les usages des gamers. Comprenant que le cloud computing arrivait à maturité et que les connexions très haut débit devenaient plus accessibles, aussi bien en fibre, en 4G et bientôt en 5G, quatre Français ont eu l’idée de créer une offre de cloud computing orientée joueurs PC dans un premier temps. Fin 2019, Shadow a lancé son offensive grand public en revoyant son offre pour élargir son public. Entre temps, Google Stadia a été dévoilé.\\nEn savoir plus sur Shadow\\n\\nContrairement à GeForce Now ou Google Stadia, il ne s’agit pas exclusivement de cloud gaming car Shadow propose l’accès à une machine complète à distance. Vous avez votre Windows, votre espace disque et vous achetez vos jeux et logiciels sur des plateformes en ligne.\\n\\nEn gros, la mission est de reproduire le fonctionnement de votre PC de bureau, à distance et de vous permettre d’y accéder confortablement sur d’autres plateformes (une interface dédiée existe sur Android TV par exemple). Audacieux, difficile à perfectionner, mais dans l’ordre du possible avec les technologies d’aujourd’hui. Et force est de reconnaître que Shadow a de l’avance par rapport à la concurrence.\\nQuelles différences avec Stadia ?\\nGoogle Stadia Premiere Edition // Source : Google\\n\\nEntre Shadow et Google Stadia, le concept au cœur du produit est le même : payer un abonnement mensuel pour accéder à un moyen de jouer à des jeux vidéo. Dans le cas de Shadow, ce moyen est un ordinateur sous Windows 10 complet. Dans le cas de Stadia par Google, c’est l’accès à une plateforme. Dans les deux cas, les jeux ne sont pas inclus dans l’abonnement. Vous achetez les jeux normalement sur les plateformes en ligne (Steam, Uplay, Origin…) sur Windows 10 sur Shadow et vous accédez au magasin Stadia sur Stadia.\\n\\nÀ partir de là, les différences sont nombreuses car Stadia n’est pas un ordinateur complet mais simplement un accès à un catalogue de jeux et la plateforme pour les faire tourner. Un article est consacré à ces questions.\\nQuelles différences avec le Xbox Game Pass ?\\n\\nOn confond assez largement un service comme Shadow ou Stadia et un service comme le Xbox Game Pass (disponible également sur PC). L’offre de Microsoft est un « Netflix du jeu vidéo » : vous payez un abonnement pour accéder à un catalogue de titres, mais c’est à vous de fournir la plateforme pour les faire tourner (votre console ou votre PC). L’offre de Shadow (ou de Google) est l’exact inverse : vous payez pour un accès à une plateforme, mais vous devrez acheter les jeux en plus.\\n\\nSi vous le souhaitez, vous pouvez bien entendu prendre un abonnement Xbox Game Pass sur un Shadow.\\nÀ quoi ressemble le boitier ?\\n\\nLe boîtier original ressemblait à cela.\\n\\nEn 2019, un nouveau boîtier nommé Ghost a été dévoilé.\\nShadow Ghost // Source : Shadow\\n\\nIl s’agit d’un petit boîtier à la connectique fort classique. On regrette l’absence d’un port micro / jack différencié, d’USB-C ou de DisplayPort en 2019.\\n\\n    1 port HDMI\\n    2 ports USB 3.0\\n    2 ports USB 2.0\\n    Un port Ethernet\\n    Connectique audio\\n\\nCela vous permet de brancher un écran, des espaces de stockage (clé USB, HDD externe etc.) et des accessoires (manettes, souris, claviers, webcam, etc.). La forme de la machine a été conçue pour s’intégrer sur un bureau et devenir invisible : on peut se débrouiller pour la cacher derrière un écran.\\n\\nSi vous souhaitez un peu d’histoire, Numerama avait eu accès aux premières versions bêta du premier boîtier, bien dénudées :\\n\\nLe SoC embarqué est capable de traiter des flux vidéo HEVC  conformes à la norme H.265, proposé en plus du H.264 par Shadow et d’exécuter le client Shadow de la plus fluide des manières. Enfin, l’engin affiche un TDP, ou enveloppe thermique globale, de 12 à 25 W.\\nQuelle(s) configuration(s) de PC ?\\n\\nFin 2019, Shadow revoit son offre et propose désormais trois configurations à trois prix.\\n\\n    Shadow Boost (jeu en 1080p) : pour 12,99 € (14,99 € sans engagement), l’utilisateur a accès à une configuration Shadow capable de lui proposer une expérience en Full HD. Elle repose sur des cartes graphiques GTX 1080, 12 Go de RAM, 256 Go de stockage et un processeur quadruple cœur à 3,4 Ghz.\\n    Shadow Ultra (jeu en 4K)  : pour 24,99 € (29,99 € sans engagement), soit à peu près le prix actuel, Shadow passe à la vitesse supérieure pour du jeu en 4K, embarquant les dernières RTX 2080, 16 Go de RAM, 512 Go de stockage et un processeur à 4 Ghz.\\n    Shadow Infinite (jeu en 4K)  : pour 39,99 € (49,99 € sans engagement), Shadow propose une expérience haut de gamme, presque inutile, mais qui correspond aux attentes d’une niche exigeante. La carte graphique est une Titan RTX, le processeur passe à 6 cœurs et la RAM à 32 Go. 1 To de stockage est inclus.\\n\\nQuels sont les clients Shadow proposés ?\\n\\nIl est possible d’accéder à sa machine depuis un client et pas seulement depuis le boîtier fourni. Les clients Windows, macOS et Android sont disponibles. Il suffit d’aller sur son compte client pour retrouver les liens. Les clients mobiles permettent d’accéder à Shadow sur des tablettes et des smartphones : confus pendant longtemps, ils ont été remplacés par des « lanceurs » de jeux très simplifiés qui ne vous montrent Windows 10 que rarement. Si vous mettez Shadow sur une Android TV, comme la Nvidia Shield, vous aurez une sorte de « console de salon avec vos jeux PC » dessus.\\nSur une tablette Google Nexus 9\\nEn savoir plus sur Shadow\\n\\nLe client Mac est l’un des plus avancés et permet de dire qu’il est possible, en 2019, d’avoir des jeux vidéo sur Mac. C’est celui que nous utilisons le plus à la rédaction. On regrette en revanche que Shadow n’ait pas intégré un mapping intelligent du clavier : copier coller un @ après avoir tapé arobase dans Google a du charme, mais ce n’est pas très pratique.\\n\\nEnfin, Shadow a pour projet d’être intégré sur les casques de réalité virtuelle autonomes (des bidouilles fonctionnent déjà), comme l’excellent Oculus Quest.\\nEst-ce une machine partagée ?\\n\\nIl y a effectivement des ressources partagées, comme le CPU et la mémoire vive, mais pour ce qui est de la carte graphique, ou du stockage, ces composants sont dédiés à votre machine.\\n\\nIl s’agit donc d’une offre différente de ce que l’on peut habituellement trouver dans les offres cloud. Pas sûr que Shadow poursuive indéfiniment sur cette voie : attribuer une carte graphique par compte est coûteux et inutile (c’est une ressource perdue quand vous ne l’utilisez pas).\\n\\nFin 2019, Shadow a annoncé un partenariat avec OVH pour le déploiement des serveurs attribués aux clients. Shadow continuera d’avoir son architecture mais peut désormais compter sur le géant français de l’hébergement pour soutenir sa croissance.\\nQuid des jeux et des logiciels ?\\n\\nLe PC est fourni avec une licence Windows 10 (incluse), et c’est tout. Vous faites ensuite ce que vous voulez avec la machine : les jeux et logiciels s’installent comme sur un vrai PC et l’utilisation que vous en faites est de votre responsabilité.\\nÀ quelle qualité d’image s’attendre ?\\n\\nLe système s’adapte à l’écran jusqu’à une définition 4K, et une fréquence de 60Hz (144Hz en Full HD). Le flux de données est encodé en H.264 ou H.265 selon une compression et une méthode qui a été raffinée depuis les premiers tests. D’ailleurs, plusieurs brevets sont (ou vont être) déposés autour des technologies mises en œuvre.\\nPhoto depuis un iPhone 7, qualité haut sur The Witcher 3\\n\\nEn bref : si votre connexion le permet, vous aurez une qualité d’image optimale, que cela soit en définition, en nombre d’images par seconde mais aussi en qualité de compression. En blind test en conditions idéales, difficile de faire la différence entre Shadow et un ordinateur local. En pratique, les aléas de votre connexion et les éléments particuliers de votre configuration peuvent entraîner ralentissements, lags ou incompatibilités.\\n\\nL’offre est disponible pour les versions ADSL 2+ et VDSL, avec quelques limites, mais reste confortable.\\nQuelle latence en jeu ?\\n\\nC’était un des points sensibles de l’affaire. Pour démontrer l’efficacité de leur solution, l’équipe de Shadow s’est tournée vers une équipe pro d’Overwatch, des joueurs de CS:GO mais aussi un des champions français de Street Fighter. Tous ces joueurs pro ont validé le concept : en conditions idéales, la latence est quasi parfaite et ce que Shadow propose ne change en rien l’expérience de jeu.\\n\\nLa latence (en fibre) est de 1 à 10 ms en fonction de votre FAI. Une fibre optique 10 Gbs/s a été installée directement vers Orange et Free. Toutes les histoires de peering ont été au cœur des problématiques de la jeune société. Chaque client a accès à un débit de 1 Gb/s asymétrique, ce qui permet de télécharger un jeu de 40 Go en quelques minutes.\\n\\nEn pratique, ce serait mentir que de dire que tout est tout le temps parfait. Shadow est dépendant de votre connexion et même une bonne fibre en termes de débit ne l’est peut-être pas en termes de latence. Le mieux reste de tester chez soi un mois sans engagement avant de craquer.\\nQuel débit pour en profiter ?\\n\\nShadow a été lancé en avant-première pour les clients équipés de la fibre optique. Fin novembre 2017, la startup a annoncé avoir travaillé sur ses algorithmes de compression et d’envoi d’un flux vidéo en temps réel pour rendre compatible sa technologie avec les foyers en ADSL 20 Mb/s. En pratique, nous avons vu de la bureautique s’afficher sans souci avec une connexion à 2 Mb/s et des jeux tourner (c’était moche mais fluide) à 5 Mb/s.\\nCombien ça coûte ?\\n\\nShadow est un abonnement à tarif dégressif selon l’engagement. Trois formules sont disponibles en précommande, avec du stockage en option.\\n\\nDu stockage en plus est disponible en option.\\nEst-ce que c’est rentable pour les joueurs ?\\n\\nBonne question. La configuration proposée est estimée entre 1 200 et 2 000 € selon l’offre. Considérons que vous ne touchez pas votre PC pendant 3 ans. Si vous vous engagez un an avec la formule Shadow la plus chère, vous payez 600 €. Vous arrivez à 1 800 € en trois ans… mais Shadow aura peut-être mis à jour ses composants sans changer le tarif. Le tarif à 12,99 € est très agressif : 3 ans d’abonnement ne vous coûtent que 468 €, soit loin du prix d’un PC convenable pour du jeu en 1080p (autour de 1 000 €).\\n\\nIl faut également prendre en compte l’économie d’énergie, relative. Une machine qui demande 150 à 250 Watts va coûter environ 100 euros par an de facture d’électricité. La petite box, elle, demande environ 15 Watts, ce qui donne environ 8 à 10 euros par an. Sur trois ans, on peut donc envisager plusieurs centaines d’euros d’économie.\\n\\nC’est une question qui a été posée de nombreuses fois : le PC sous Windows 10 mis à disposition est régi par les mêmes lois qui régissent les données dans le cloud. Les machines sont situées à Marcoussis en France ou dans les data centers d’OVH avec les nouvelles offres.\\nEn haut à gauche, le débit alloué à Shadow\\n\\nIl faudra donc protéger votre PC, comme si c’était un « vrai » PC : anti-virus, anti-malware, pare-feu… vous connaissez la chanson. Le flux vidéo, lui, est chiffré.\\n\\nEnfin, les fondateurs de l’entreprise sont plutôt sereins : les hackers devront défaire la défense du data center, puis ensuite trouver un vecteur d’attaque contre la ou les machine(s) ciblée(s). Chaque machine proposant une protection différente, il paraît donc encore plus difficile de hacker une machine Shadow qu’une machine locale.\\nQuelles sont les alternatives de cloud computing orientées gamers ?\\n\\nIl existe plusieurs options : des solutions BtoB, du jeu à la demande, du cloud computing… Shadow n’est pas le premier à se lancer dans l’aventure et le cloud computing existe depuis au moins 30 ans.\\n\\nNvidia propose son service GeForce Now sur la Shield Android TV par exemple. Les géants de la tech et du web se lancent aussi dans le cloud gaming — Google avec Stadia, Microsoft avec xCloud.\\n\\nDe son côté, Sony a racheté la société Gaikai et récupéré des brevets à la fermeture d’OnLive pour mettre sur pied son PlayStation Now. Celui-ci permet de jouer à environ 150 jeux PS3 sur PS4 en streaming.\\nQuand est-ce que cela sera disponible ?\\n\\nShadow est disponible sur www.shadow.tech. Les nouvelles offres sont prévues pour 2020.\\nQu’en pensons-nous ?\\n\\nNous avons testé la première version de Shadow qui nous a impressionnée par sa réactivité. C’est un produit déjà mature et qui pourra convenir à plus d’un joueur ou d’une joueuse. En 2018, Shadow a eu quelques soucis avec la montée en charge de ses serveurs à cause du succès du produit. À la mi 2018, la plupart de ces problèmes étaient réglés. Reste qu’en 2019, les soucis viennent principalement de la connexion et des bugs du client. Fin 2019, nos derniers tests sont beaucoup plus concluants.\\n\\nReste que notre conseil tient toujours : testez bien chez vous un mois sans engagement avant de vous lancer !\\n\"}]],\"sections\":[[10,0]]}','<div class=\"kg-card-markdown\"><p>Voici une présentation générale par Numerama du cloud computing français Shadow. <a href=\"https://www.numerama.com/tech/203045-shadow-tout-savoir-sur-le-pc-du-futur-en-19-questions.html\">Lien de l\'article</a>.</p>\n<p>Alors que le cloud gaming commence à prendre plusieurs formes chez les géants de la tech, les Français de Shadow avancent. Fin 2019, l\'entreprise a bouleversé ses offres pour conquérir le grand public. Pour comprendre comment fonctionne ce PC dans le cloud étonnant, nous vous proposons une FAQ complète.</p>\n<p>Dossier mis à jour le 19/04/2019.<br>\nShadow : Qu’est-ce que c’est ?</p>\n<p>Simple : un ordinateur sous Windows 10, à distance, dans le cloud, pensé pour les usages des gamers. Comprenant que le cloud computing arrivait à maturité et que les connexions très haut débit devenaient plus accessibles, aussi bien en fibre, en 4G et bientôt en 5G, quatre Français ont eu l’idée de créer une offre de cloud computing orientée joueurs PC dans un premier temps. Fin 2019, Shadow a lancé son offensive grand public en revoyant son offre pour élargir son public. Entre temps, Google Stadia a été dévoilé.<br>\nEn savoir plus sur Shadow</p>\n<p>Contrairement à GeForce Now ou Google Stadia, il ne s’agit pas exclusivement de cloud gaming car Shadow propose l’accès à une machine complète à distance. Vous avez votre Windows, votre espace disque et vous achetez vos jeux et logiciels sur des plateformes en ligne.</p>\n<p>En gros, la mission est de reproduire le fonctionnement de votre PC de bureau, à distance et de vous permettre d’y accéder confortablement sur d’autres plateformes (une interface dédiée existe sur Android TV par exemple). Audacieux, difficile à perfectionner, mais dans l’ordre du possible avec les technologies d’aujourd’hui. Et force est de reconnaître que Shadow a de l’avance par rapport à la concurrence.<br>\nQuelles différences avec Stadia ?<br>\nGoogle Stadia Premiere Edition // Source : Google</p>\n<p>Entre Shadow et Google Stadia, le concept au cœur du produit est le même : payer un abonnement mensuel pour accéder à un moyen de jouer à des jeux vidéo. Dans le cas de Shadow, ce moyen est un ordinateur sous Windows 10 complet. Dans le cas de Stadia par Google, c’est l’accès à une plateforme. Dans les deux cas, les jeux ne sont pas inclus dans l’abonnement. Vous achetez les jeux normalement sur les plateformes en ligne (Steam, Uplay, Origin…) sur Windows 10 sur Shadow et vous accédez au magasin Stadia sur Stadia.</p>\n<p>À partir de là, les différences sont nombreuses car Stadia n’est pas un ordinateur complet mais simplement un accès à un catalogue de jeux et la plateforme pour les faire tourner. Un article est consacré à ces questions.<br>\nQuelles différences avec le Xbox Game Pass ?</p>\n<p>On confond assez largement un service comme Shadow ou Stadia et un service comme le Xbox Game Pass (disponible également sur PC). L’offre de Microsoft est un « Netflix du jeu vidéo » : vous payez un abonnement pour accéder à un catalogue de titres, mais c’est à vous de fournir la plateforme pour les faire tourner (votre console ou votre PC). L’offre de Shadow (ou de Google) est l’exact inverse : vous payez pour un accès à une plateforme, mais vous devrez acheter les jeux en plus.</p>\n<p>Si vous le souhaitez, vous pouvez bien entendu prendre un abonnement Xbox Game Pass sur un Shadow.<br>\nÀ quoi ressemble le boitier ?</p>\n<p>Le boîtier original ressemblait à cela.</p>\n<p>En 2019, un nouveau boîtier nommé Ghost a été dévoilé.<br>\nShadow Ghost // Source : Shadow</p>\n<p>Il s’agit d’un petit boîtier à la connectique fort classique. On regrette l’absence d’un port micro / jack différencié, d’USB-C ou de DisplayPort en 2019.</p>\n<pre><code>1 port HDMI\n2 ports USB 3.0\n2 ports USB 2.0\nUn port Ethernet\nConnectique audio\n</code></pre>\n<p>Cela vous permet de brancher un écran, des espaces de stockage (clé USB, HDD externe etc.) et des accessoires (manettes, souris, claviers, webcam, etc.). La forme de la machine a été conçue pour s’intégrer sur un bureau et devenir invisible : on peut se débrouiller pour la cacher derrière un écran.</p>\n<p>Si vous souhaitez un peu d’histoire, Numerama avait eu accès aux premières versions bêta du premier boîtier, bien dénudées :</p>\n<p>Le SoC embarqué est capable de traiter des flux vidéo HEVC  conformes à la norme H.265, proposé en plus du H.264 par Shadow et d’exécuter le client Shadow de la plus fluide des manières. Enfin, l’engin affiche un TDP, ou enveloppe thermique globale, de 12 à 25 W.<br>\nQuelle(s) configuration(s) de PC ?</p>\n<p>Fin 2019, Shadow revoit son offre et propose désormais trois configurations à trois prix.</p>\n<pre><code>Shadow Boost (jeu en 1080p) : pour 12,99 € (14,99 € sans engagement), l’utilisateur a accès à une configuration Shadow capable de lui proposer une expérience en Full HD. Elle repose sur des cartes graphiques GTX 1080, 12 Go de RAM, 256 Go de stockage et un processeur quadruple cœur à 3,4 Ghz.\nShadow Ultra (jeu en 4K)  : pour 24,99 € (29,99 € sans engagement), soit à peu près le prix actuel, Shadow passe à la vitesse supérieure pour du jeu en 4K, embarquant les dernières RTX 2080, 16 Go de RAM, 512 Go de stockage et un processeur à 4 Ghz.\nShadow Infinite (jeu en 4K)  : pour 39,99 € (49,99 € sans engagement), Shadow propose une expérience haut de gamme, presque inutile, mais qui correspond aux attentes d’une niche exigeante. La carte graphique est une Titan RTX, le processeur passe à 6 cœurs et la RAM à 32 Go. 1 To de stockage est inclus.\n</code></pre>\n<p>Quels sont les clients Shadow proposés ?</p>\n<p>Il est possible d’accéder à sa machine depuis un client et pas seulement depuis le boîtier fourni. Les clients Windows, macOS et Android sont disponibles. Il suffit d’aller sur son compte client pour retrouver les liens. Les clients mobiles permettent d’accéder à Shadow sur des tablettes et des smartphones : confus pendant longtemps, ils ont été remplacés par des « lanceurs » de jeux très simplifiés qui ne vous montrent Windows 10 que rarement. Si vous mettez Shadow sur une Android TV, comme la Nvidia Shield, vous aurez une sorte de « console de salon avec vos jeux PC » dessus.<br>\nSur une tablette Google Nexus 9<br>\nEn savoir plus sur Shadow</p>\n<p>Le client Mac est l’un des plus avancés et permet de dire qu’il est possible, en 2019, d’avoir des jeux vidéo sur Mac. C’est celui que nous utilisons le plus à la rédaction. On regrette en revanche que Shadow n’ait pas intégré un mapping intelligent du clavier : copier coller un @ après avoir tapé arobase dans Google a du charme, mais ce n’est pas très pratique.</p>\n<p>Enfin, Shadow a pour projet d’être intégré sur les casques de réalité virtuelle autonomes (des bidouilles fonctionnent déjà), comme l’excellent Oculus Quest.<br>\nEst-ce une machine partagée ?</p>\n<p>Il y a effectivement des ressources partagées, comme le CPU et la mémoire vive, mais pour ce qui est de la carte graphique, ou du stockage, ces composants sont dédiés à votre machine.</p>\n<p>Il s’agit donc d’une offre différente de ce que l’on peut habituellement trouver dans les offres cloud. Pas sûr que Shadow poursuive indéfiniment sur cette voie : attribuer une carte graphique par compte est coûteux et inutile (c’est une ressource perdue quand vous ne l’utilisez pas).</p>\n<p>Fin 2019, Shadow a annoncé un partenariat avec OVH pour le déploiement des serveurs attribués aux clients. Shadow continuera d’avoir son architecture mais peut désormais compter sur le géant français de l’hébergement pour soutenir sa croissance.<br>\nQuid des jeux et des logiciels ?</p>\n<p>Le PC est fourni avec une licence Windows 10 (incluse), et c’est tout. Vous faites ensuite ce que vous voulez avec la machine : les jeux et logiciels s’installent comme sur un vrai PC et l’utilisation que vous en faites est de votre responsabilité.<br>\nÀ quelle qualité d’image s’attendre ?</p>\n<p>Le système s’adapte à l’écran jusqu’à une définition 4K, et une fréquence de 60Hz (144Hz en Full HD). Le flux de données est encodé en H.264 ou H.265 selon une compression et une méthode qui a été raffinée depuis les premiers tests. D’ailleurs, plusieurs brevets sont (ou vont être) déposés autour des technologies mises en œuvre.<br>\nPhoto depuis un iPhone 7, qualité haut sur The Witcher 3</p>\n<p>En bref : si votre connexion le permet, vous aurez une qualité d’image optimale, que cela soit en définition, en nombre d’images par seconde mais aussi en qualité de compression. En blind test en conditions idéales, difficile de faire la différence entre Shadow et un ordinateur local. En pratique, les aléas de votre connexion et les éléments particuliers de votre configuration peuvent entraîner ralentissements, lags ou incompatibilités.</p>\n<p>L’offre est disponible pour les versions ADSL 2+ et VDSL, avec quelques limites, mais reste confortable.<br>\nQuelle latence en jeu ?</p>\n<p>C’était un des points sensibles de l’affaire. Pour démontrer l’efficacité de leur solution, l’équipe de Shadow s’est tournée vers une équipe pro d’Overwatch, des joueurs de CS:GO mais aussi un des champions français de Street Fighter. Tous ces joueurs pro ont validé le concept : en conditions idéales, la latence est quasi parfaite et ce que Shadow propose ne change en rien l’expérience de jeu.</p>\n<p>La latence (en fibre) est de 1 à 10 ms en fonction de votre FAI. Une fibre optique 10 Gbs/s a été installée directement vers Orange et Free. Toutes les histoires de peering ont été au cœur des problématiques de la jeune société. Chaque client a accès à un débit de 1 Gb/s asymétrique, ce qui permet de télécharger un jeu de 40 Go en quelques minutes.</p>\n<p>En pratique, ce serait mentir que de dire que tout est tout le temps parfait. Shadow est dépendant de votre connexion et même une bonne fibre en termes de débit ne l’est peut-être pas en termes de latence. Le mieux reste de tester chez soi un mois sans engagement avant de craquer.<br>\nQuel débit pour en profiter ?</p>\n<p>Shadow a été lancé en avant-première pour les clients équipés de la fibre optique. Fin novembre 2017, la startup a annoncé avoir travaillé sur ses algorithmes de compression et d’envoi d’un flux vidéo en temps réel pour rendre compatible sa technologie avec les foyers en ADSL 20 Mb/s. En pratique, nous avons vu de la bureautique s’afficher sans souci avec une connexion à 2 Mb/s et des jeux tourner (c’était moche mais fluide) à 5 Mb/s.<br>\nCombien ça coûte ?</p>\n<p>Shadow est un abonnement à tarif dégressif selon l’engagement. Trois formules sont disponibles en précommande, avec du stockage en option.</p>\n<p>Du stockage en plus est disponible en option.<br>\nEst-ce que c’est rentable pour les joueurs ?</p>\n<p>Bonne question. La configuration proposée est estimée entre 1 200 et 2 000 € selon l’offre. Considérons que vous ne touchez pas votre PC pendant 3 ans. Si vous vous engagez un an avec la formule Shadow la plus chère, vous payez 600 €. Vous arrivez à 1 800 € en trois ans… mais Shadow aura peut-être mis à jour ses composants sans changer le tarif. Le tarif à 12,99 € est très agressif : 3 ans d’abonnement ne vous coûtent que 468 €, soit loin du prix d’un PC convenable pour du jeu en 1080p (autour de 1 000 €).</p>\n<p>Il faut également prendre en compte l’économie d’énergie, relative. Une machine qui demande 150 à 250 Watts va coûter environ 100 euros par an de facture d’électricité. La petite box, elle, demande environ 15 Watts, ce qui donne environ 8 à 10 euros par an. Sur trois ans, on peut donc envisager plusieurs centaines d’euros d’économie.</p>\n<p>C’est une question qui a été posée de nombreuses fois : le PC sous Windows 10 mis à disposition est régi par les mêmes lois qui régissent les données dans le cloud. Les machines sont situées à Marcoussis en France ou dans les data centers d’OVH avec les nouvelles offres.<br>\nEn haut à gauche, le débit alloué à Shadow</p>\n<p>Il faudra donc protéger votre PC, comme si c’était un « vrai » PC : anti-virus, anti-malware, pare-feu… vous connaissez la chanson. Le flux vidéo, lui, est chiffré.</p>\n<p>Enfin, les fondateurs de l’entreprise sont plutôt sereins : les hackers devront défaire la défense du data center, puis ensuite trouver un vecteur d’attaque contre la ou les machine(s) ciblée(s). Chaque machine proposant une protection différente, il paraît donc encore plus difficile de hacker une machine Shadow qu’une machine locale.<br>\nQuelles sont les alternatives de cloud computing orientées gamers ?</p>\n<p>Il existe plusieurs options : des solutions BtoB, du jeu à la demande, du cloud computing… Shadow n’est pas le premier à se lancer dans l’aventure et le cloud computing existe depuis au moins 30 ans.</p>\n<p>Nvidia propose son service GeForce Now sur la Shield Android TV par exemple. Les géants de la tech et du web se lancent aussi dans le cloud gaming — Google avec Stadia, Microsoft avec xCloud.</p>\n<p>De son côté, Sony a racheté la société Gaikai et récupéré des brevets à la fermeture d’OnLive pour mettre sur pied son PlayStation Now. Celui-ci permet de jouer à environ 150 jeux PS3 sur PS4 en streaming.<br>\nQuand est-ce que cela sera disponible ?</p>\n<p>Shadow est disponible sur www.shadow.tech. Les nouvelles offres sont prévues pour 2020.<br>\nQu’en pensons-nous ?</p>\n<p>Nous avons testé la première version de Shadow qui nous a impressionnée par sa réactivité. C’est un produit déjà mature et qui pourra convenir à plus d’un joueur ou d’une joueuse. En 2018, Shadow a eu quelques soucis avec la montée en charge de ses serveurs à cause du succès du produit. À la mi 2018, la plupart de ces problèmes étaient réglés. Reste qu’en 2019, les soucis viennent principalement de la connexion et des bugs du client. Fin 2019, nos derniers tests sont beaucoup plus concluants.</p>\n<p>Reste que notre conseil tient toujours : testez bien chez vous un mois sans engagement avant de vous lancer !</p>\n</div>',NULL,'Voici une présentation générale par Numerama du cloud computing français Shadow.\n Lien de l\'article\n[https://www.numerama.com/tech/203045-shadow-tout-savoir-sur-le-pc-du-futur-en-19-questions.html]\n.\n\nAlors que le cloud gaming commence à prendre plusieurs formes chez les géants de\nla tech, les Français de Shadow avancent. Fin 2019, l\'entreprise a bouleversé\nses offres pour conquérir le grand public. Pour comprendre comment fonctionne ce\nPC dans le cloud étonnant, nous vous proposons une FAQ complète.\n\nDossier mis à jour le 19/04/2019.\nShadow : Qu’est-ce que c’est ?\n\nSimple : un ordinateur sous Windows 10, à distance, dans le cloud, pensé pour\nles usages des gamers. Comprenant que le cloud computing arrivait à maturité et\nque les connexions très haut débit devenaient plus accessibles, aussi bien en\nfibre, en 4G et bientôt en 5G, quatre Français ont eu l’idée de créer une offre\nde cloud computing orientée joueurs PC dans un premier temps. Fin 2019, Shadow a\nlancé son offensive grand public en revoyant son offre pour élargir son public.\nEntre temps, Google Stadia a été dévoilé.\nEn savoir plus sur Shadow\n\nContrairement à GeForce Now ou Google Stadia, il ne s’agit pas exclusivement de\ncloud gaming car Shadow propose l’accès à une machine complète à distance. Vous\navez votre Windows, votre espace disque et vous achetez vos jeux et logiciels\nsur des plateformes en ligne.\n\nEn gros, la mission est de reproduire le fonctionnement de votre PC de bureau, à\ndistance et de vous permettre d’y accéder confortablement sur d’autres\nplateformes (une interface dédiée existe sur Android TV par exemple). Audacieux,\ndifficile à perfectionner, mais dans l’ordre du possible avec les technologies\nd’aujourd’hui. Et force est de reconnaître que Shadow a de l’avance par rapport\nà la concurrence.\nQuelles différences avec Stadia ?\nGoogle Stadia Premiere Edition // Source : Google\n\nEntre Shadow et Google Stadia, le concept au cœur du produit est le même : payer\nun abonnement mensuel pour accéder à un moyen de jouer à des jeux vidéo. Dans le\ncas de Shadow, ce moyen est un ordinateur sous Windows 10 complet. Dans le cas\nde Stadia par Google, c’est l’accès à une plateforme. Dans les deux cas, les\njeux ne sont pas inclus dans l’abonnement. Vous achetez les jeux normalement sur\nles plateformes en ligne (Steam, Uplay, Origin…) sur Windows 10 sur Shadow et\nvous accédez au magasin Stadia sur Stadia.\n\nÀ partir de là, les différences sont nombreuses car Stadia n’est pas un\nordinateur complet mais simplement un accès à un catalogue de jeux et la\nplateforme pour les faire tourner. Un article est consacré à ces questions.\nQuelles différences avec le Xbox Game Pass ?\n\nOn confond assez largement un service comme Shadow ou Stadia et un service comme\nle Xbox Game Pass (disponible également sur PC). L’offre de Microsoft est un «\nNetflix du jeu vidéo » : vous payez un abonnement pour accéder à un catalogue de\ntitres, mais c’est à vous de fournir la plateforme pour les faire tourner (votre\nconsole ou votre PC). L’offre de Shadow (ou de Google) est l’exact inverse :\nvous payez pour un accès à une plateforme, mais vous devrez acheter les jeux en\nplus.\n\nSi vous le souhaitez, vous pouvez bien entendu prendre un abonnement Xbox Game\nPass sur un Shadow.\nÀ quoi ressemble le boitier ?\n\nLe boîtier original ressemblait à cela.\n\nEn 2019, un nouveau boîtier nommé Ghost a été dévoilé.\nShadow Ghost // Source : Shadow\n\nIl s’agit d’un petit boîtier à la connectique fort classique. On regrette\nl’absence d’un port micro / jack différencié, d’USB-C ou de DisplayPort en 2019.\n\n1 port HDMI\n2 ports USB 3.0\n2 ports USB 2.0\nUn port Ethernet\nConnectique audio\n\n\nCela vous permet de brancher un écran, des espaces de stockage (clé USB, HDD\nexterne etc.) et des accessoires (manettes, souris, claviers, webcam, etc.). La\nforme de la machine a été conçue pour s’intégrer sur un bureau et devenir\ninvisible : on peut se débrouiller pour la cacher derrière un écran.\n\nSi vous souhaitez un peu d’histoire, Numerama avait eu accès aux premières\nversions bêta du premier boîtier, bien dénudées :\n\nLe SoC embarqué est capable de traiter des flux vidéo HEVC conformes à la norme\nH.265, proposé en plus du H.264 par Shadow et d’exécuter le client Shadow de la\nplus fluide des manières. Enfin, l’engin affiche un TDP, ou enveloppe thermique\nglobale, de 12 à 25 W.\nQuelle(s) configuration(s) de PC ?\n\nFin 2019, Shadow revoit son offre et propose désormais trois configurations à\ntrois prix.\n\nShadow Boost (jeu en 1080p) : pour 12,99 € (14,99 € sans engagement), l’utilisateur a accès à une configuration Shadow capable de lui proposer une expérience en Full HD. Elle repose sur des cartes graphiques GTX 1080, 12 Go de RAM, 256 Go de stockage et un processeur quadruple cœur à 3,4 Ghz.\nShadow Ultra (jeu en 4K)  : pour 24,99 € (29,99 € sans engagement), soit à peu près le prix actuel, Shadow passe à la vitesse supérieure pour du jeu en 4K, embarquant les dernières RTX 2080, 16 Go de RAM, 512 Go de stockage et un processeur à 4 Ghz.\nShadow Infinite (jeu en 4K)  : pour 39,99 € (49,99 € sans engagement), Shadow propose une expérience haut de gamme, presque inutile, mais qui correspond aux attentes d’une niche exigeante. La carte graphique est une Titan RTX, le processeur passe à 6 cœurs et la RAM à 32 Go. 1 To de stockage est inclus.\n\n\nQuels sont les clients Shadow proposés ?\n\nIl est possible d’accéder à sa machine depuis un client et pas seulement depuis\nle boîtier fourni. Les clients Windows, macOS et Android sont disponibles. Il\nsuffit d’aller sur son compte client pour retrouver les liens. Les clients\nmobiles permettent d’accéder à Shadow sur des tablettes et des smartphones :\nconfus pendant longtemps, ils ont été remplacés par des « lanceurs » de jeux\ntrès simplifiés qui ne vous montrent Windows 10 que rarement. Si vous mettez\nShadow sur une Android TV, comme la Nvidia Shield, vous aurez une sorte de «\nconsole de salon avec vos jeux PC » dessus.\nSur une tablette Google Nexus 9\nEn savoir plus sur Shadow\n\nLe client Mac est l’un des plus avancés et permet de dire qu’il est possible, en\n2019, d’avoir des jeux vidéo sur Mac. C’est celui que nous utilisons le plus à\nla rédaction. On regrette en revanche que Shadow n’ait pas intégré un mapping\nintelligent du clavier : copier coller un @ après avoir tapé arobase dans Google\na du charme, mais ce n’est pas très pratique.\n\nEnfin, Shadow a pour projet d’être intégré sur les casques de réalité virtuelle\nautonomes (des bidouilles fonctionnent déjà), comme l’excellent Oculus Quest.\nEst-ce une machine partagée ?\n\nIl y a effectivement des ressources partagées, comme le CPU et la mémoire vive,\nmais pour ce qui est de la carte graphique, ou du stockage, ces composants sont\ndédiés à votre machine.\n\nIl s’agit donc d’une offre différente de ce que l’on peut habituellement trouver\ndans les offres cloud. Pas sûr que Shadow poursuive indéfiniment sur cette voie\n: attribuer une carte graphique par compte est coûteux et inutile (c’est une\nressource perdue quand vous ne l’utilisez pas).\n\nFin 2019, Shadow a annoncé un partenariat avec OVH pour le déploiement des\nserveurs attribués aux clients. Shadow continuera d’avoir son architecture mais\npeut désormais compter sur le géant français de l’hébergement pour soutenir sa\ncroissance.\nQuid des jeux et des logiciels ?\n\nLe PC est fourni avec une licence Windows 10 (incluse), et c’est tout. Vous\nfaites ensuite ce que vous voulez avec la machine : les jeux et logiciels\ns’installent comme sur un vrai PC et l’utilisation que vous en faites est de\nvotre responsabilité.\nÀ quelle qualité d’image s’attendre ?\n\nLe système s’adapte à l’écran jusqu’à une définition 4K, et une fréquence de\n60Hz (144Hz en Full HD). Le flux de données est encodé en H.264 ou H.265 selon\nune compression et une méthode qui a été raffinée depuis les premiers tests.\nD’ailleurs, plusieurs brevets sont (ou vont être) déposés autour des\ntechnologies mises en œuvre.\nPhoto depuis un iPhone 7, qualité haut sur The Witcher 3\n\nEn bref : si votre connexion le permet, vous aurez une qualité d’image optimale,\nque cela soit en définition, en nombre d’images par seconde mais aussi en\nqualité de compression. En blind test en conditions idéales, difficile de faire\nla différence entre Shadow et un ordinateur local. En pratique, les aléas de\nvotre connexion et les éléments particuliers de votre configuration peuvent\nentraîner ralentissements, lags ou incompatibilités.\n\nL’offre est disponible pour les versions ADSL 2+ et VDSL, avec quelques limites,\nmais reste confortable.\nQuelle latence en jeu ?\n\nC’était un des points sensibles de l’affaire. Pour démontrer l’efficacité de\nleur solution, l’équipe de Shadow s’est tournée vers une équipe pro d’Overwatch,\ndes joueurs de CS:GO mais aussi un des champions français de Street Fighter.\nTous ces joueurs pro ont validé le concept : en conditions idéales, la latence\nest quasi parfaite et ce que Shadow propose ne change en rien l’expérience de\njeu.\n\nLa latence (en fibre) est de 1 à 10 ms en fonction de votre FAI. Une fibre\noptique 10 Gbs/s a été installée directement vers Orange et Free. Toutes les\nhistoires de peering ont été au cœur des problématiques de la jeune société.\nChaque client a accès à un débit de 1 Gb/s asymétrique, ce qui permet de\ntélécharger un jeu de 40 Go en quelques minutes.\n\nEn pratique, ce serait mentir que de dire que tout est tout le temps parfait.\nShadow est dépendant de votre connexion et même une bonne fibre en termes de\ndébit ne l’est peut-être pas en termes de latence. Le mieux reste de tester chez\nsoi un mois sans engagement avant de craquer.\nQuel débit pour en profiter ?\n\nShadow a été lancé en avant-première pour les clients équipés de la fibre\noptique. Fin novembre 2017, la startup a annoncé avoir travaillé sur ses\nalgorithmes de compression et d’envoi d’un flux vidéo en temps réel pour rendre\ncompatible sa technologie avec les foyers en ADSL 20 Mb/s. En pratique, nous\navons vu de la bureautique s’afficher sans souci avec une connexion à 2 Mb/s et\ndes jeux tourner (c’était moche mais fluide) à 5 Mb/s.\nCombien ça coûte ?\n\nShadow est un abonnement à tarif dégressif selon l’engagement. Trois formules\nsont disponibles en précommande, avec du stockage en option.\n\nDu stockage en plus est disponible en option.\nEst-ce que c’est rentable pour les joueurs ?\n\nBonne question. La configuration proposée est estimée entre 1 200 et 2 000 €\nselon l’offre. Considérons que vous ne touchez pas votre PC pendant 3 ans. Si\nvous vous engagez un an avec la formule Shadow la plus chère, vous payez 600 €.\nVous arrivez à 1 800 € en trois ans… mais Shadow aura peut-être mis à jour ses\ncomposants sans changer le tarif. Le tarif à 12,99 € est très agressif : 3 ans\nd’abonnement ne vous coûtent que 468 €, soit loin du prix d’un PC convenable\npour du jeu en 1080p (autour de 1 000 €).\n\nIl faut également prendre en compte l’économie d’énergie, relative. Une machine\nqui demande 150 à 250 Watts va coûter environ 100 euros par an de facture\nd’électricité. La petite box, elle, demande environ 15 Watts, ce qui donne\nenviron 8 à 10 euros par an. Sur trois ans, on peut donc envisager plusieurs\ncentaines d’euros d’économie.\n\nC’est une question qui a été posée de nombreuses fois : le PC sous Windows 10\nmis à disposition est régi par les mêmes lois qui régissent les données dans le\ncloud. Les machines sont situées à Marcoussis en France ou dans les data centers\nd’OVH avec les nouvelles offres.\nEn haut à gauche, le débit alloué à Shadow\n\nIl faudra donc protéger votre PC, comme si c’était un « vrai » PC : anti-virus,\nanti-malware, pare-feu… vous connaissez la chanson. Le flux vidéo, lui, est\nchiffré.\n\nEnfin, les fondateurs de l’entreprise sont plutôt sereins : les hackers devront\ndéfaire la défense du data center, puis ensuite trouver un vecteur d’attaque\ncontre la ou les machine(s) ciblée(s). Chaque machine proposant une protection\ndifférente, il paraît donc encore plus difficile de hacker une machine Shadow\nqu’une machine locale.\nQuelles sont les alternatives de cloud computing orientées gamers ?\n\nIl existe plusieurs options : des solutions BtoB, du jeu à la demande, du cloud\ncomputing… Shadow n’est pas le premier à se lancer dans l’aventure et le cloud\ncomputing existe depuis au moins 30 ans.\n\nNvidia propose son service GeForce Now sur la Shield Android TV par exemple. Les\ngéants de la tech et du web se lancent aussi dans le cloud gaming — Google avec\nStadia, Microsoft avec xCloud.\n\nDe son côté, Sony a racheté la société Gaikai et récupéré des brevets à la\nfermeture d’OnLive pour mettre sur pied son PlayStation Now. Celui-ci permet de\njouer à environ 150 jeux PS3 sur PS4 en streaming.\nQuand est-ce que cela sera disponible ?\n\nShadow est disponible sur www.shadow.tech. Les nouvelles offres sont prévues\npour 2020.\nQu’en pensons-nous ?\n\nNous avons testé la première version de Shadow qui nous a impressionnée par sa\nréactivité. C’est un produit déjà mature et qui pourra convenir à plus d’un\njoueur ou d’une joueuse. En 2018, Shadow a eu quelques soucis avec la montée en\ncharge de ses serveurs à cause du succès du produit. À la mi 2018, la plupart de\nces problèmes étaient réglés. Reste qu’en 2019, les soucis viennent\nprincipalement de la connexion et des bugs du client. Fin 2019, nos derniers\ntests sont beaucoup plus concluants.\n\nReste que notre conseil tient toujours : testez bien chez vous un mois sans\nengagement avant de vous lancer !','/content/images/2020/03/numerama-une.jpg',0,0,'published',NULL,'public',NULL,NULL,'1','2020-03-11 17:27:46','1','2020-03-11 17:51:51','1','2020-03-11 17:27:51','1',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e6925535816ef0001601d0a','077ee5fc-5347-4be9-bfa3-19fe9a6e29df','[CLOUDWARDS] Review Cloud Gaming Shadow','cloudwards','{\"version\":\"0.3.1\",\"markups\":[],\"atoms\":[],\"cards\":[[\"card-markdown\",{\"cardName\":\"card-markdown\",\"markdown\":\"[Voici un article](https://www.cloudwards.net/shadow-review/) présentant tous les points forts et les points faibles du service Shadow de la société Blade.\\n\\nForce:\\n\\n    Bonne performance\\n    Bureau Windows 10\\n    Tous les jeux pc virtualisés acceptés\\n    Application Shadow sur Android & iOS\\n    Facile d\'utilisation\\n\\nWeaknesses:\\n\\n    Disponibilité limitée\\n    Tarif conséquent\\n    Manque de contrôle sur les applications mobiles\\n    \\n \\n\"}]],\"sections\":[[10,0]]}','<div class=\"kg-card-markdown\"><p><a href=\"https://www.cloudwards.net/shadow-review/\">Voici un article</a> présentant tous les points forts et les points faibles du service Shadow de la société Blade.</p>\n<p>Force:</p>\n<pre><code>Bonne performance\nBureau Windows 10\nTous les jeux pc virtualisés acceptés\nApplication Shadow sur Android &amp; iOS\nFacile d\'utilisation\n</code></pre>\n<p>Weaknesses:</p>\n<pre><code>Disponibilité limitée\nTarif conséquent\nManque de contrôle sur les applications mobiles\n</code></pre>\n</div>',NULL,'Voici un article [https://www.cloudwards.net/shadow-review/]  présentant tous\nles points forts et les points faibles du service Shadow de la société Blade.\n\nForce:\n\nBonne performance\nBureau Windows 10\nTous les jeux pc virtualisés acceptés\nApplication Shadow sur Android & iOS\nFacile d\'utilisation\n\n\nWeaknesses:\n\nDisponibilité limitée\nTarif conséquent\nManque de contrôle sur les applications mobiles','/content/images/2020/03/cloudwards.jpg',0,0,'published',NULL,'public',NULL,NULL,'1','2020-03-11 17:52:19','1','2020-03-11 18:07:57','1','2020-03-11 18:06:56','1',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e6929835816ef0001601d0c','2c40db9a-b030-4529-9b09-3982818194e3','[DIGITALTRENDS] Tout ce qu\'il y a savoir sur Shadow','post-post','{\"version\":\"0.3.1\",\"markups\":[],\"atoms\":[],\"cards\":[[\"card-markdown\",{\"cardName\":\"card-markdown\",\"markdown\":\"Ici, un article présentant Shadow dans sa généralité.\\n\\nQuel matériel est utilisé dans les salles serveurs ?\\n\\nQuelle type de connexion est nécessaire pour ce type de service ?\\n\\nQuels jeux sont supportés sur cette plateforme ?\\n\\nQuels sont les tarifs pour ce service ?\\n\\nLes réponses à ces questions en suivant [ce lien](https://www.digitaltrends.com/gaming/shadow-game-streaming-price-availability-features/).\\n\"}]],\"sections\":[[10,0]]}','<div class=\"kg-card-markdown\"><p>Ici, un article présentant Shadow dans sa généralité.</p>\n<p>Quel matériel est utilisé dans les salles serveurs ?</p>\n<p>Quelle type de connexion est nécessaire pour ce type de service ?</p>\n<p>Quels jeux sont supportés sur cette plateforme ?</p>\n<p>Quels sont les tarifs pour ce service ?</p>\n<p>Les réponses à ces questions en suivant <a href=\"https://www.digitaltrends.com/gaming/shadow-game-streaming-price-availability-features/\">ce lien</a>.</p>\n</div>',NULL,'Ici, un article présentant Shadow dans sa généralité.\n\nQuel matériel est utilisé dans les salles serveurs ?\n\nQuelle type de connexion est nécessaire pour ce type de service ?\n\nQuels jeux sont supportés sur cette plateforme ?\n\nQuels sont les tarifs pour ce service ?\n\nLes réponses à ces questions en suivant ce lien\n[https://www.digitaltrends.com/gaming/shadow-game-streaming-price-availability-features/]\n.','/content/images/2020/03/digitaltrends.png',0,0,'published',NULL,'public',NULL,NULL,'1','2020-03-11 18:10:11','1','2020-03-11 18:19:25','1','2020-03-11 18:19:25','1',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e692e6e5816ef0001601d0e','99b1b22b-c53b-4b95-9eec-61b0c497c4d7','[FRANDROID] Un retard de SHADOW début février 2020','frandroid-un-retard-de-shadow-debut-fevrier-2020','{\"version\":\"0.3.1\",\"markups\":[],\"atoms\":[],\"cards\":[[\"card-markdown\",{\"cardName\":\"card-markdown\",\"markdown\":\"[Lien vers l\'article FRANDROID](https://www.frandroid.com/produits-android/console/674581_shadow-ultra-et-infinite-de-mars-a-2021-voici-le-calendrier-des-activations-retardees).\\n\\nShadow a fait subir plusieurs retard à ses clients dû au Coronavirus et à une mauvaise gestion des précommandes des nouveaux utilisateurs du cloud computing de la société Blade.\\n\\nSi vous avez précommandé un Shadow Ultra ou un Shadow Infinite, il y a beaucoup d’informations à connaître sur les activations, selon si vous êtes un nouveau ou un ancien utilisateur. Tout d’abord, ce qu’il faut retenir c’est que seulement 10 % des précommandes Ultra et Infinite seront activées dans le courant du mois de mars.\\n\\nParmi ces heureux élus, deux tiers seront d’anciens clients de l’entreprise profitant déjà d’une offre Shadow et le reste des sélectionnés seront de nouveaux utilisateurs. Yann Carron justifie ce panachage en expliquant que la firme française a besoin de données variées sur plusieurs scénarios d’utilisation afin de convaincre les investisseurs et obtenir plus de fonds.\\n\\nReste donc 90 % des précommandes de Shadow Ultra et de Shadow Infinite qui accuseront un retard. Pour tous ces utilisateurs laissés temporairement sur la touche, le représentant explique que l’objectif est de lancer les activations dès « la fin de l’été prochain ». Il promet aussi de fournir un maximum d’informations d’ici là.\\n\\nDans une précédente vidéo diffusée en direct sur Twitch, d’autres responsables de Shadow avaient expliqué que ces retards étaient dus à une succession de petits problèmes qu’il a fallu régler un par un (carte mère, refroidissement, détection du GPU…). Cela a pris plus de temps que prévu, car les correctifs reposent sur le changement de plusieurs composants qui peuvent avoir un gros impact sur le fonctionnement des machines virtuelles.\\n\\nEn outre, les équipes techniques doivent à chaque fois s’assurer que les réparations qu’elles effectuent fonctionnent pour l’ensemble des machines gérées par leurs serveurs.\\n\\nL’entreprise française joue aussi de malchance avec les boîtiers Shadow Ghost. Normalement, la livraison des produits est prévue pour la moitié du mois de mars, mais il est possible qu’un retard survienne ici également à cause du coronavirus. En effet, les fournisseurs chinois de la firme assurent que les appareils sont prêts, mais qu’il est pour l’instant impossible de les tester et de les empaqueter.\"}]],\"sections\":[[10,0]]}','<div class=\"kg-card-markdown\"><p><a href=\"https://www.frandroid.com/produits-android/console/674581_shadow-ultra-et-infinite-de-mars-a-2021-voici-le-calendrier-des-activations-retardees\">Lien vers l\'article FRANDROID</a>.</p>\n<p>Shadow a fait subir plusieurs retard à ses clients dû au Coronavirus et à une mauvaise gestion des précommandes des nouveaux utilisateurs du cloud computing de la société Blade.</p>\n<p>Si vous avez précommandé un Shadow Ultra ou un Shadow Infinite, il y a beaucoup d’informations à connaître sur les activations, selon si vous êtes un nouveau ou un ancien utilisateur. Tout d’abord, ce qu’il faut retenir c’est que seulement 10 % des précommandes Ultra et Infinite seront activées dans le courant du mois de mars.</p>\n<p>Parmi ces heureux élus, deux tiers seront d’anciens clients de l’entreprise profitant déjà d’une offre Shadow et le reste des sélectionnés seront de nouveaux utilisateurs. Yann Carron justifie ce panachage en expliquant que la firme française a besoin de données variées sur plusieurs scénarios d’utilisation afin de convaincre les investisseurs et obtenir plus de fonds.</p>\n<p>Reste donc 90 % des précommandes de Shadow Ultra et de Shadow Infinite qui accuseront un retard. Pour tous ces utilisateurs laissés temporairement sur la touche, le représentant explique que l’objectif est de lancer les activations dès « la fin de l’été prochain ». Il promet aussi de fournir un maximum d’informations d’ici là.</p>\n<p>Dans une précédente vidéo diffusée en direct sur Twitch, d’autres responsables de Shadow avaient expliqué que ces retards étaient dus à une succession de petits problèmes qu’il a fallu régler un par un (carte mère, refroidissement, détection du GPU…). Cela a pris plus de temps que prévu, car les correctifs reposent sur le changement de plusieurs composants qui peuvent avoir un gros impact sur le fonctionnement des machines virtuelles.</p>\n<p>En outre, les équipes techniques doivent à chaque fois s’assurer que les réparations qu’elles effectuent fonctionnent pour l’ensemble des machines gérées par leurs serveurs.</p>\n<p>L’entreprise française joue aussi de malchance avec les boîtiers Shadow Ghost. Normalement, la livraison des produits est prévue pour la moitié du mois de mars, mais il est possible qu’un retard survienne ici également à cause du coronavirus. En effet, les fournisseurs chinois de la firme assurent que les appareils sont prêts, mais qu’il est pour l’instant impossible de les tester et de les empaqueter.</p>\n</div>',NULL,'Lien vers l\'article FRANDROID\n[https://www.frandroid.com/produits-android/console/674581_shadow-ultra-et-infinite-de-mars-a-2021-voici-le-calendrier-des-activations-retardees]\n.\n\nShadow a fait subir plusieurs retard à ses clients dû au Coronavirus et à une\nmauvaise gestion des précommandes des nouveaux utilisateurs du cloud computing\nde la société Blade.\n\nSi vous avez précommandé un Shadow Ultra ou un Shadow Infinite, il y a beaucoup\nd’informations à connaître sur les activations, selon si vous êtes un nouveau ou\nun ancien utilisateur. Tout d’abord, ce qu’il faut retenir c’est que seulement\n10 % des précommandes Ultra et Infinite seront activées dans le courant du mois\nde mars.\n\nParmi ces heureux élus, deux tiers seront d’anciens clients de l’entreprise\nprofitant déjà d’une offre Shadow et le reste des sélectionnés seront de\nnouveaux utilisateurs. Yann Carron justifie ce panachage en expliquant que la\nfirme française a besoin de données variées sur plusieurs scénarios\nd’utilisation afin de convaincre les investisseurs et obtenir plus de fonds.\n\nReste donc 90 % des précommandes de Shadow Ultra et de Shadow Infinite qui\naccuseront un retard. Pour tous ces utilisateurs laissés temporairement sur la\ntouche, le représentant explique que l’objectif est de lancer les activations\ndès « la fin de l’été prochain ». Il promet aussi de fournir un maximum\nd’informations d’ici là.\n\nDans une précédente vidéo diffusée en direct sur Twitch, d’autres responsables\nde Shadow avaient expliqué que ces retards étaient dus à une succession de\npetits problèmes qu’il a fallu régler un par un (carte mère, refroidissement,\ndétection du GPU…). Cela a pris plus de temps que prévu, car les correctifs\nreposent sur le changement de plusieurs composants qui peuvent avoir un gros\nimpact sur le fonctionnement des machines virtuelles.\n\nEn outre, les équipes techniques doivent à chaque fois s’assurer que les\nréparations qu’elles effectuent fonctionnent pour l’ensemble des machines gérées\npar leurs serveurs.\n\nL’entreprise française joue aussi de malchance avec les boîtiers Shadow Ghost.\nNormalement, la livraison des produits est prévue pour la moitié du mois de\nmars, mais il est possible qu’un retard survienne ici également à cause du\ncoronavirus. En effet, les fournisseurs chinois de la firme assurent que les\nappareils sont prêts, mais qu’il est pour l’instant impossible de les tester et\nde les empaqueter.','/content/images/2020/03/cloud-gaming-frandroid-dsc03736-1200x800.jpg',0,0,'published',NULL,'public',NULL,NULL,'1','2020-03-11 18:31:10','1','2020-03-11 18:39:58','1','2020-03-11 18:39:58','1',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e6932d35816ef0001601d10','e990b91e-9dd4-4032-bdba-18fd515854b1','[LECAFEDUGEEK] SHADOW retiré de l\'AppStore (APPLE)','lecafedugeek-shadow-retire-de-lappstore-apple','{\"version\":\"0.3.1\",\"markups\":[],\"atoms\":[],\"cards\":[[\"card-markdown\",{\"cardName\":\"card-markdown\",\"markdown\":\"[Lien vers l\'article en entier.](https://lecafedugeek.fr/shadow-son-application-a-ete-retiree-de-lapp-store/)\\n\\nLe PC dans le cloud Shadow disparait de l’App Store\\n\\nShadow était pour le moment le seul service de « cloud gaming » (même s’il s’agit plutôt de cloud computing ici) disponible sur iOS/iPad OS et tvOS. Seulement, l’application dédiée pour ces plateformes vient d’être supprimée par Apple. En effet, à travers un message sur Reddit, Blade explique que l’application violerait une partie des règles de l’App Store. Ainsi, que ce soit pour la version stable ou la version en bêta, elles ne sont plus disponibles pour ses utilisateurs.\\n\\nApple et Shadow n’ont pas donné plus de détails sur quelle partie des règles l’application pose problème, mais on imagine qu’il est question ici d’un souci sur la fameuse marge de 30% qu’Apple récupère habituellement. Pour rappel, quand vous faites un achat via l’App Store (ex. achats intégrés), Apple récupère 30% du prix total. Or, via Shadow, les utilisateurs pouvaient acheter des logiciels ou des jeux sans passer par l’App Store. Par conséquent, la multinationale ne touchait pas la marge de 30%. Heureusement, pas de panique si vous avez installé l’application avant sa suppression, elle fonctionnera toujours aussi bien comme avant. Pour les autres, l’entreprise travaille sur ce sujet pour remettre l’application en ligne dès que possible. Plus de détails dans notre article dédié.\"}]],\"sections\":[[10,0]]}','<div class=\"kg-card-markdown\"><p><a href=\"https://lecafedugeek.fr/shadow-son-application-a-ete-retiree-de-lapp-store/\">Lien vers l\'article en entier.</a></p>\n<p>Le PC dans le cloud Shadow disparait de l’App Store</p>\n<p>Shadow était pour le moment le seul service de « cloud gaming » (même s’il s’agit plutôt de cloud computing ici) disponible sur iOS/iPad OS et tvOS. Seulement, l’application dédiée pour ces plateformes vient d’être supprimée par Apple. En effet, à travers un message sur Reddit, Blade explique que l’application violerait une partie des règles de l’App Store. Ainsi, que ce soit pour la version stable ou la version en bêta, elles ne sont plus disponibles pour ses utilisateurs.</p>\n<p>Apple et Shadow n’ont pas donné plus de détails sur quelle partie des règles l’application pose problème, mais on imagine qu’il est question ici d’un souci sur la fameuse marge de 30% qu’Apple récupère habituellement. Pour rappel, quand vous faites un achat via l’App Store (ex. achats intégrés), Apple récupère 30% du prix total. Or, via Shadow, les utilisateurs pouvaient acheter des logiciels ou des jeux sans passer par l’App Store. Par conséquent, la multinationale ne touchait pas la marge de 30%. Heureusement, pas de panique si vous avez installé l’application avant sa suppression, elle fonctionnera toujours aussi bien comme avant. Pour les autres, l’entreprise travaille sur ce sujet pour remettre l’application en ligne dès que possible. Plus de détails dans notre article dédié.</p>\n</div>',NULL,'Lien vers l\'article en entier.\n[https://lecafedugeek.fr/shadow-son-application-a-ete-retiree-de-lapp-store/]\n\nLe PC dans le cloud Shadow disparait de l’App Store\n\nShadow était pour le moment le seul service de « cloud gaming » (même s’il\ns’agit plutôt de cloud computing ici) disponible sur iOS/iPad OS et tvOS.\nSeulement, l’application dédiée pour ces plateformes vient d’être supprimée par\nApple. En effet, à travers un message sur Reddit, Blade explique que\nl’application violerait une partie des règles de l’App Store. Ainsi, que ce soit\npour la version stable ou la version en bêta, elles ne sont plus disponibles\npour ses utilisateurs.\n\nApple et Shadow n’ont pas donné plus de détails sur quelle partie des règles\nl’application pose problème, mais on imagine qu’il est question ici d’un souci\nsur la fameuse marge de 30% qu’Apple récupère habituellement. Pour rappel, quand\nvous faites un achat via l’App Store (ex. achats intégrés), Apple récupère 30%\ndu prix total. Or, via Shadow, les utilisateurs pouvaient acheter des logiciels\nou des jeux sans passer par l’App Store. Par conséquent, la multinationale ne\ntouchait pas la marge de 30%. Heureusement, pas de panique si vous avez installé\nl’application avant sa suppression, elle fonctionnera toujours aussi bien comme\navant. Pour les autres, l’entreprise travaille sur ce sujet pour remettre\nl’application en ligne dès que possible. Plus de détails dans notre article\ndédié.','/content/images/2020/03/Le-caf--du-geek-Paypite-presse.png',0,0,'published',NULL,'public',NULL,NULL,'1','2020-03-11 18:49:55','1','2020-03-11 18:54:55','1','2020-03-11 18:54:55','1',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_authors`
--

DROP TABLE IF EXISTS `posts_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_authors` (
  `id` varchar(24) NOT NULL,
  `post_id` varchar(24) NOT NULL,
  `author_id` varchar(24) NOT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `posts_authors_post_id_foreign` (`post_id`),
  KEY `posts_authors_author_id_foreign` (`author_id`),
  CONSTRAINT `posts_authors_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  CONSTRAINT `posts_authors_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_authors`
--

LOCK TABLES `posts_authors` WRITE;
/*!40000 ALTER TABLE `posts_authors` DISABLE KEYS */;
INSERT INTO `posts_authors` VALUES ('5e6915ff28571c00011d77ba','5e69152028571c00011d77b8','5951f5fca366002ebd5dbef7',0),('5e691f925816ef0001601d09','5e691f925816ef0001601d08','1',0),('5e6925535816ef0001601d0b','5e6925535816ef0001601d0a','1',0),('5e6929835816ef0001601d0d','5e6929835816ef0001601d0c','1',0),('5e692e6e5816ef0001601d0f','5e692e6e5816ef0001601d0e','1',0),('5e6932d35816ef0001601d11','5e6932d35816ef0001601d10','1',0);
/*!40000 ALTER TABLE `posts_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_tags`
--

DROP TABLE IF EXISTS `posts_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_tags` (
  `id` varchar(24) NOT NULL,
  `post_id` varchar(24) NOT NULL,
  `tag_id` varchar(24) NOT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `posts_tags_post_id_foreign` (`post_id`),
  KEY `posts_tags_tag_id_foreign` (`tag_id`),
  CONSTRAINT `posts_tags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  CONSTRAINT `posts_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_tags`
--

LOCK TABLES `posts_tags` WRITE;
/*!40000 ALTER TABLE `posts_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refreshtokens`
--

DROP TABLE IF EXISTS `refreshtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refreshtokens` (
  `id` varchar(24) NOT NULL,
  `token` varchar(191) NOT NULL,
  `user_id` varchar(24) NOT NULL,
  `client_id` varchar(24) NOT NULL,
  `expires` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `refreshtokens_token_unique` (`token`),
  KEY `refreshtokens_user_id_foreign` (`user_id`),
  KEY `refreshtokens_client_id_foreign` (`client_id`),
  CONSTRAINT `refreshtokens_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `refreshtokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refreshtokens`
--

LOCK TABLES `refreshtokens` WRITE;
/*!40000 ALTER TABLE `refreshtokens` DISABLE KEYS */;
INSERT INTO `refreshtokens` VALUES ('5e68ccd50b82c0000143c8b8','R7iAT66foHhPyvRhQTSCLuTL49w7FL53AAxYp7XJwSxmqnuaQItghGvON9QaWcSGil51E5jsmM4x0HkfOsIJDKCh1wTmpRGfiBXjWtOkRswJG8B4ra4fZP4qB6tN42igxFIbvSOFgMsQqUfEcFail4FJi7pWCDfDoLXTpona3GHufto0vXTUrDeUht8Qfg4','1','5e68cb29b58cfd0017b57f7b',1599694485566),('5e68fc3cbd6094000111bc6d','yPzAXmqWAUFMp39liKdhrRbfmq5QmjjpS7UU4RtVDCv0eEwKZ6qk9E3w6VrrZSUAIhz6fMKcyVnb39POuDYieF75xF75kYtJHLffBwTmPwSKpcDXWOSRZ5NYwnBOWbaOXE3Pe8E8pFxYtZAdQ6p9c5cRKeEqAxOPzEmtrgag6K2DvftAxcWFbPDpaKQt5jn','1','5e68cb29b58cfd0017b57f7b',1599706620685),('5e6905c928571c00011d77b3','5nLSl0Mk61qAzWcEsxlY5qtRRvTypOSKb3D6EtL8c0SnAp7OXq1NwCSg8Dwrsr73ZdjxDGSLJ4vD5nv8NYKEzTn1nSykjbHC3NRQe5tzsucnZfq2rXAHv1Nc8EhIzzK3T0b3wJZXDKKEjTPMz5eMX64PVerT8m8u8hYEolrkTTQnOaPIbrAug5kdB09yy4D','1','5e68cb29b58cfd0017b57f7b',1599710828269),('5e6913b728571c00011d77b6','lI9lzYaOGKl18FDfyBL4BVih0Vc7H11scixcoxj14N7C2qqPx3llOzaQHuLe1n3owSoRADF4iWHTqrqzJwlzD5n22kCD9DbNiOduswMRoCgzSCK9AlYvMvYdx6IVojbBz7nkx2kK4jQCoVxplqXxS1e1bbVtjR2tdAhk4NQhfXbk6yZiD7VWJScl819aUMx','1','5e68cb29b58cfd0017b57f7b',1599712631378),('5e691e985816ef0001601d06','zfBMsmuTWjHX9gTCYxd8UQWhaD6OzcCnyCgeygqg68HPuggbXNgVSfe4Ozdr3mm1FA0MnNNmpm2kyBSdT5qOSNqjnCQYBjgRbla5AjRxOyQLSBknaG9XEDtYvhwFnsc21sjLdWPOuTjccumwgHDrFqzEZ1q3XBmHUE88iSjUlkNO70oO6M1IItkaiqParre','1','5e68cb29b58cfd0017b57f7b',1599720806357);
/*!40000 ALTER TABLE `refreshtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` varchar(24) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('5e68cb29b58cfd0017b57f7f','Administrator','Administrators','2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f80','Editor','Editors','2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f81','Author','Authors','2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f82','Contributor','Contributors','2020-03-11 11:27:37','1','2020-03-11 11:27:37','1'),('5e68cb29b58cfd0017b57f83','Owner','Blog Owner','2020-03-11 11:27:37','1','2020-03-11 11:27:37','1');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_users` (
  `id` varchar(24) NOT NULL,
  `role_id` varchar(24) NOT NULL,
  `user_id` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` VALUES ('5e68cb2ab58cfd0017b57fb9','5e68cb29b58cfd0017b57f81','5951f5fca366002ebd5dbef7'),('5e68cb2ab58cfd0017b5804b','5e68cb29b58cfd0017b57f83','1');
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` varchar(24) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text,
  `type` varchar(50) NOT NULL DEFAULT 'core',
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('5e68cb2c0b82c0000143c89c','db_hash','fdacbc84-873f-4c0e-8b3b-3cb6cb14a854','core','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c89d','next_update_check','1584033808','core','2020-03-11 11:27:40','1','2020-03-11 17:23:27','1'),('5e68cb2c0b82c0000143c89e','notifications','[{\"dismissible\":true,\"location\":\"bottom\",\"status\":\"alert\",\"id\":\"045f7e0c-5305-44bf-8b32-6f955d461234\",\"custom\":true,\"createdAt\":\"2018-08-21T19:07:32.000Z\",\"type\":\"info\",\"top\":true,\"message\":\"<strong>Ghost 2.0 is now available</strong> You are currently using an old version of Ghost which means you don\'t have access to the latest features. <a href=\\\"https://blog.ghost.org/2-0/\\\" target=\\\"_blank\\\" rel=\\\"noopener\\\">Read more!</a>\",\"seen\":true,\"addedAt\":\"2020-03-11T11:27:51.335Z\"}]','core','2020-03-11 11:27:40','1','2020-03-11 11:34:54','1'),('5e68cb2c0b82c0000143c89f','title','Shadow','blog','2020-03-11 11:27:40','1','2020-03-11 17:26:26','1'),('5e68cb2c0b82c0000143c8a0','description','Veille technologique','blog','2020-03-11 11:27:40','1','2020-03-11 17:24:56','1'),('5e68cb2c0b82c0000143c8a1','logo','','blog','2020-03-11 11:27:40','1','2020-03-11 17:26:55','1'),('5e68cb2c0b82c0000143c8a2','cover_image','/content/images/2020/03/Sans-titre.png','blog','2020-03-11 11:27:40','1','2020-03-11 17:24:56','1'),('5e68cb2c0b82c0000143c8a3','icon','','blog','2020-03-11 11:27:40','1','2020-03-11 16:18:21','1'),('5e68cb2c0b82c0000143c8a4','default_locale','en','blog','2020-03-11 11:27:40','1','2020-03-11 17:25:51','1'),('5e68cb2c0b82c0000143c8a5','active_timezone','Etc/UTC','blog','2020-03-11 11:27:40','1','2020-03-11 17:24:56','1'),('5e68cb2c0b82c0000143c8a6','force_i18n','true','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8a7','permalinks','/:slug/','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8a8','amp','true','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8a9','ghost_head','','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8aa','ghost_foot','','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8ab','facebook','','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8ac','twitter','@ShadowVeille','blog','2020-03-11 11:27:40','1','2020-03-11 17:27:16','1'),('5e68cb2c0b82c0000143c8ad','labs','{\"publicAPI\": true}','blog','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8ae','navigation','[{\"label\":\"Home\",\"url\":\"/\"}]','blog','2020-03-11 11:27:40','1','2020-03-11 15:39:08','1'),('5e68cb2c0b82c0000143c8af','slack','[]','blog','2020-03-11 11:27:40','1','2020-03-11 15:39:08','1'),('5e68cb2c0b82c0000143c8b0','unsplash','{\"isActive\":true}','blog','2020-03-11 11:27:40','1','2020-03-11 15:39:08','1'),('5e68cb2c0b82c0000143c8b1','active_theme','casper','theme','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8b2','active_apps','[]','app','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8b3','installed_apps','[]','app','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8b4','is_private','false','private','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8b5','password','','private','2020-03-11 11:27:40','1','2020-03-11 11:27:40','1'),('5e68cb2c0b82c0000143c8b6','public_hash','695d79a73811b73b69903bc3b2ff7f','private','2020-03-11 11:27:40','1','2020-03-11 17:24:56','1');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` varchar(24) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `post_id` varchar(24) DEFAULT NULL,
  `subscribed_url` varchar(2000) DEFAULT NULL,
  `subscribed_referrer` varchar(2000) DEFAULT NULL,
  `unsubscribed_url` varchar(2000) DEFAULT NULL,
  `unsubscribed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribers_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` varchar(24) NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `description` text,
  `feature_image` varchar(2000) DEFAULT NULL,
  `parent_id` varchar(191) DEFAULT NULL,
  `visibility` varchar(50) NOT NULL DEFAULT 'public',
  `meta_title` varchar(2000) DEFAULT NULL,
  `meta_description` varchar(2000) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES ('5e68cb29b58cfd0017b57f7a','Getting Started','getting-started',NULL,NULL,NULL,'public',NULL,NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(24) NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `ghost_auth_access_token` varchar(32) DEFAULT NULL,
  `ghost_auth_id` varchar(24) DEFAULT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(191) NOT NULL,
  `profile_image` varchar(2000) DEFAULT NULL,
  `cover_image` varchar(2000) DEFAULT NULL,
  `bio` text,
  `website` varchar(2000) DEFAULT NULL,
  `location` text,
  `facebook` varchar(2000) DEFAULT NULL,
  `twitter` varchar(2000) DEFAULT NULL,
  `accessibility` text,
  `status` varchar(50) NOT NULL DEFAULT 'active',
  `locale` varchar(6) DEFAULT NULL,
  `visibility` varchar(50) NOT NULL DEFAULT 'public',
  `meta_title` varchar(2000) DEFAULT NULL,
  `meta_description` varchar(2000) DEFAULT NULL,
  `tour` text,
  `last_seen` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_slug_unique` (`slug`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','Thibaut','thibaut',NULL,NULL,'$2a$10$jIr3IiUbtxCim7kuQpS2yuEfRtdub8TOKM47jSXs9O7OqFEgPP2Aq','thibautrichard8@outlook.fr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{\"nightShift\":false}','active',NULL,'public',NULL,NULL,'[\"getting-started\",\"using-the-editor\",\"featured-post\",\"static-post\"]','2020-03-11 17:23:36','2020-03-11 11:27:37','1','2020-03-11 17:23:36','1'),('5951f5fca366002ebd5dbef7','Ghost','ghost',NULL,NULL,'$2a$10$liJv2PAjl.K76HVh7wmtyOad1GwAMlWdL6K/SGSeuSphZOq92RJIO','ghost-author@example.com',NULL,NULL,'You can delete this user to remove all the welcome posts',NULL,NULL,NULL,NULL,NULL,'active',NULL,'public',NULL,NULL,NULL,NULL,'2020-03-11 11:27:37','1','2020-03-11 11:27:37','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webhooks`
--

DROP TABLE IF EXISTS `webhooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webhooks` (
  `id` varchar(24) NOT NULL,
  `event` varchar(50) NOT NULL,
  `target_url` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(24) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webhooks`
--

LOCK TABLES `webhooks` WRITE;
/*!40000 ALTER TABLE `webhooks` DISABLE KEYS */;
/*!40000 ALTER TABLE `webhooks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-11 19:11:18
